<?php

if (!isset($_SESSION)) {
	session_start();
}

require_once './src/Roots.php';
require_once PATH_SRC . '/autoloader/Autoloader.php';
Autoloader::registrar();

$pagSinSesion = array(
	"app/presentacion/clave.php",
	"app/presentacion/carrito.php",
	"app/presentacion/pedido.php",
	"app/presentacion/permisoCliente.php",
	"app/presentacion/permisoDom.php",
	"app/presentacion/excepciones.php",
	"app/presentacion/permisoAdmin.php",
	"app/presentacion/sinSesion.php",
	"app/presentacion/crear.php",
	"app/presentacion/crearCliente.php",
	"app/presentacion/inicio.php",
	"app/presentacion/autenticar.php"

);

$pagConSesion = array(
	"app/presentacion/editPerfilCliente.php"
);

$pid = null;
if (isset($_GET["pid"])) {
	$pid = base64_decode($_GET["pid"]);
}

$priv = null;
if (isset($_GET["priv"])) {
	$priv = base64_decode(base64_decode($_GET["priv"]));
}
?>

<!doctype html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<link href="https://bootswatch.com/4/sketchy/bootstrap.css" rel="stylesheet" />
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
	<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
	<script src="https://pagination.js.org/dist/2.1.5/pagination.min.js"></script>

	<script src=https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.12.7/sweetalert2.min.js></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	<link rel="stylesheet" type="text/css" href="app/css/estilo.css">


	<title>JD Comics</title>
	<link rel="icon" type="image/png" href="app/img/J.png">
</head>

<body class="contenedor" <?php echo 'style="' . ((isset($_SESSION["id"]) && $_SESSION["rol"] == "administrador") || (isset($_SESSION["id"]) && $_SESSION["rol"] == "domiciliario") ? 'background: linear-gradient(to bottom, gray, white);background-attachment:fixed;' : 'background-color: white;') . '"'; ?>>
	<?php
	if (isset($pid)) {
		if (isset($_SESSION["id"])) {
			if ($_SESSION["rol"] == "administrador") {
				include "app/presentacion/menuAdministrador.php";
			} else if ($_SESSION["rol"] == "cliente") {
				if (in_array($pid, $pagSinSesion) || in_array($pid, $pagConSesion)) {
					echo "";
				} else {
					include "app/presentacion/inicio.php";
				}
			} else if ($_SESSION["rol"] == "domiciliario") {
				include "app/presentacion/menuDomiciliario.php";
			} else {
				include "app/presentacion/inicio.php";
			}
			include $pid;
		} else if (in_array($pid, $pagSinSesion)) {
			include $pid;
		} else {
			header("Location: index.php");
		}
	} else {
		include "app/presentacion/inicio.php";
	}


	if (isset($pid)) {
		if (isset($_SESSION["id"])) {
			if ($_SESSION["rol"] != "administrador") {
				include "app/presentacion/footer.php";
			}
		} else {
			include "app/presentacion/footer.php";
		}
	} else {
		include "app/presentacion/footer.php";
	}



	?>

</body>

</html>


<?php
if ( !empty($_SESSION["error"]) ) {
?>
	<script>
		Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: ' <?php echo $_SESSION["error"] ?>'
		})
	</script>
	
<?php

}





<?php

class ControladorPedido{

    public function insertarPedido($data){
        $pedidoModel= new Pedido();
       $id=$pedidoModel->insert($data);
        
     $v= ($id>0);
     $respuesta = new Respuesta( $v ? EMensaje::INSERCION_EXITOSA : EMensaje::ERROR_INSERSION);
     $respuesta->setDatos($id);
     return $respuesta;
    }

    public function pedidoExistente ($id )
    {
       $pedidoModel= new Pedido();
       $pedidoModel->where("Domiciliario_idDomiciliario","=",$id);
       $pedidoModel->where("estado","=","1");
       $pedido=$pedidoModel->first("idPedido");
      
       $v= ($pedido);
       $respuesta = new Respuesta( $v ? EMensaje::LOGIN_EXITOSO : EMensaje::ERROR_LOGIN);
       $respuesta->setDatos($pedido);
       return $respuesta;
    }



    public function pedidoOtorgado ($id )
    {
       $pedidoModel= new Pedido();
       $pedidoModel->where("Domiciliario_idDomiciliario","=",$id);
       $pedidoModel->where("estado","=","1");
       $pedido=$pedidoModel->first("carrito,idPedido");
      
       $v= ($pedido);
       $respuesta = new Respuesta( $v ? EMensaje::LOGIN_EXITOSO : EMensaje::ERROR_LOGIN);
       $respuesta->setDatos($pedido);
       return $respuesta;
    }

    public function actualizar($pedido){
        $pedidoModel= new Pedido();
        $actualizados =$pedidoModel->where("idPedido","=", $pedido["idPedido"])
        ->update($pedido);
        $v= ($actualizados>0);
        $respuesta = new Respuesta( $v ? EMensaje::ACTUALIZACION_EXITOSA : EMensaje::ERROR_ACTUALIZACION);
        $respuesta->setDatos($actualizados);
        return $respuesta;
     }



     public function actualizarPerCarrito($pedido){
      $pedidoModel= new Pedido();
      $actualizados =$pedidoModel->where("carrito","=", $pedido["idCarrito"])
      ->update($pedido);
      $v= ($actualizados>0);
      $respuesta = new Respuesta( $v ? EMensaje::ACTUALIZACION_EXITOSA : EMensaje::ERROR_ACTUALIZACION);
      $respuesta->setDatos($actualizados);
      return $respuesta;
   }


     public function listarPag($estado,$cantidad, $pagina, $orden, $dir){
        $pedidoModel= new Pedido();
        if(intval($estado)>0)
        {
          $pedidoModel->where("estado","=",$estado);
        }
        
    
        $lista=$pedidoModel->getPag("*",$cantidad, $pagina, $orden, $dir);
        $v= (count($lista));
        $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
        $respuesta->setDatos($lista);
        return $respuesta;
     }
  
     public function listarReg($estado){
        $pedidoModel= new Pedido();
        if(intval($estado)>0)
        {
          $pedidoModel->where("estado","=",$estado);
        }
        $lista=$pedidoModel->get("idPedido");
        $v= (count($lista));
        $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
        $respuesta->setDatos($v);
        return $respuesta;
     }


     public function listarRegPersonal($id){
        $pedidoModel= new Pedido();
        $pedidoModel->where("Domiciliario_idDomiciliario","=",$id);
        $pedidoModel->where("estado","=",2);
        $pedidoModel->orWhere("estado","=",3);
        $lista=$pedidoModel->get("idPedido");
        $v= (count($lista));
        $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
        $respuesta->setDatos($v);
        return $respuesta;
     }

     public function pedidoPersonal($id,$cantidad, $pagina, $orden, $dir){
        $pedidoModel= new Pedido();
   
        $pedidoModel->where("Domiciliario_idDomiciliario","=",$id);
        $pedidoModel->where("estado",">","1");
       
        $lista=$pedidoModel->getPag("*",$cantidad, $pagina, $orden, $dir);
        $v= (count($lista));
        $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
        $respuesta->setDatos($lista);
        return $respuesta;
     }




}

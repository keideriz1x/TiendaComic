<?php
class ControladorCarrito{

    function __constructor() { 
		  
    }
    public function insertarCarrito($carrito){
        $carritoModel= new Carrito();
       $id=$carritoModel->insert($carrito);
        
     $v= ($id>0);
     $respuesta = new Respuesta( $v ? EMensaje::INSERCION_EXITOSA : EMensaje::ERROR_INSERSION);
     $respuesta->setDatos($id);
     return $respuesta;
    }


    public function actualizar($carrito){
        $carritoModel= new Carrito();
        $actualizados =$carritoModel->where("idCarrito","=", $carrito["idCarrito"])
        ->update($carrito);
        $v= ($actualizados>0);
        $respuesta = new Respuesta( $v ? EMensaje::ACTUALIZACION_EXITOSA : EMensaje::ERROR_ACTUALIZACION);
        $respuesta->setDatos($actualizados);
        return $respuesta;
     }


    public function listarPag($estado,$cantidad, $pagina, $orden, $dir){
        $carritoModel= new Carrito();
        if(intval($estado)>0)
        {
          $carritoModel->where("estado","=",$estado);
        }
        
    
        $lista=$carritoModel->getPag("*",$cantidad, $pagina, $orden, $dir);
        $v= (count($lista));
        $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
        $respuesta->setDatos($lista);
        return $respuesta;
     }
  
     public function listarReg($estado){
        $carritoModel= new Carrito();
        $carritoModel->where("estado","=",$estado);
        $lista=$carritoModel->get("idCarrito");
        $v= (count($lista));
        $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
        $respuesta->setDatos($v);
        return $respuesta;
     }


     public function carritoPersonal($id,$cantidad, $pagina, $orden, $dir){
        $carritoModel= new Carrito();
   
         $carritoModel->where("Cliente_idCliente","=",$id);
        $lista=$carritoModel->getPag("*",$cantidad, $pagina, $orden, $dir);
        $v= (count($lista));
        $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
        $respuesta->setDatos($lista);
        return $respuesta;
     }






     public function listarRegPersonal($id){
      $carritoModel= new Carrito();
      $carritoModel->where("Cliente_idCliente","=",$id);
      $lista=$carritoModel->get("idCarrito");
      $v= (count($lista));
      $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
      $respuesta->setDatos($v);
      return $respuesta;
   }
  

   public function carritoOtorgado ($id)
   {
      $pedidoModel= new Carrito();
      $pedidoModel->where("idCarrito","=",$id);
      // $pedidoModel->where("estado","=","1");
      $pedido=$pedidoModel->first("*");
     
      $v= ($pedido);
      $respuesta = new Respuesta( $v ? EMensaje::LOGIN_EXITOSO : EMensaje::ERROR_LOGIN);
      $respuesta->setDatos($pedido);
      return $respuesta;
   }


}
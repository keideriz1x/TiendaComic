<?php

class ControladorCliente{

    function __constructor() { 
		  
    }
    
 public function insertarCliente($cliente){


  
     $clienteModel= new Cliente();
     $id =$clienteModel->insert($cliente);

    
     $v= ($id>0);
      $respuesta = new Respuesta( $v ? EMensaje::INSERCION_EXITOSA : EMensaje::ERROR_INSERSION);
      $respuesta->setDatos($id);
      return $respuesta;

  
   
    }
     public function listarCliente(){
        $clienteModel= new Cliente();
        $lista=$clienteModel->get("idCliente,nombre,apellido,correo,direccion,telefono,estado");
        $v= (count($lista));
        $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
        $respuesta->setDatos($lista);
        return $respuesta;

     }
   


     public function listarPag($cantidad, $pagina, $orden, $dir){
      $clienteModel= new Cliente();
      $lista=$clienteModel->getPag("idCliente,nombre,apellido,correo,direccion,telefono,estado",$cantidad, $pagina, $orden, $dir);
      $v= (count($lista));
      $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
      $respuesta->setDatos($lista);
      return $respuesta;
   }

   public function listarReg(){
      $clienteModel= new Cliente();
      $lista=$clienteModel->get("idCliente");
      $v= (count($lista));
      $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
      $respuesta->setDatos($v);
      return $respuesta;
   }


     public function actualizar($cliente){
        $clienteModel= new Cliente();
        $actualizados =$clienteModel->where("idCliente","=", $cliente["idCliente"])
        ->update($cliente);
        $v= ($actualizados>0);
        $respuesta = new Respuesta( $v ? EMensaje::ACTUALIZACION_EXITOSA : EMensaje::ERROR_ACTUALIZACION);
        $respuesta->setDatos($actualizados);
        return $respuesta;
     }

     public function loginCliente ($correo )
     {
        $clienteModel= new Cliente();
        $clienteModel->where("correo","=",$correo);
        // $clienteModel->where("clave","=",$clave);
        $cliente=$clienteModel->first("idCliente,nombre,apellido,foto,estado,clave,correo,direccion,telefono");
       
        $v= ($cliente);
        $respuesta = new Respuesta( $v ? EMensaje::LOGIN_EXITOSO : EMensaje::ERROR_LOGIN);
        $respuesta->setDatos($cliente);
        return $respuesta;
     }

     public function buscarCliente($dato)
     {
        $clienteModel= new Cliente();
        $clienteModel->where("nombre","LIKE",$dato.'%');
        $clienteModel->orWhere("apellido","LIKE",$dato.'%');
        $cliente=$clienteModel->get("idCliente,nombre,apellido,correo,direccion,telefono,estado");
        $v= ($cliente);
        $respuesta = new Respuesta( $v ? EMensaje::CORREO_CORRECTO : EMensaje::ERROR_CORREO);
        $respuesta->setDatos($cliente);
        return $respuesta;
     }



     
     public function cargarInformacion ($id )
     {
        $clienteModel= new Cliente();
        $clienteModel->where("idCliente","=",$id);
        
        $cliente=$clienteModel->first("nombre,apellido,correo,foto,direccion,telefono");
       
        $v= ($cliente);
        $respuesta = new Respuesta( $v ? EMensaje::LOGIN_EXITOSO : EMensaje::ERROR_LOGIN);
        $respuesta->setDatos($cliente);
        return $respuesta;
     }


     public function verificarClave ($id)
     {
        $clienteModel= new Cliente();
        $clienteModel->where("idCliente","=",$id);
        $cliente=$clienteModel->first("clave");
        $v= ($cliente);
        $respuesta = new Respuesta( $v ? EMensaje::CORREO_CORRECTO : EMensaje::ERROR_CORREO);
        $respuesta->setDatos($cliente);
        return $respuesta;
     }


     public function foto ($id )
     {
        $clienteModel= new Cliente();
        $clienteModel->where("idCliente","=",$id);
        
        $cliente=$clienteModel->first("foto");
       
        $v= ($cliente);
        $respuesta = new Respuesta( $v ? EMensaje::LOGIN_EXITOSO : EMensaje::ERROR_LOGIN);
        $respuesta->setDatos($cliente);
        return $respuesta;
     }
     
}


<?php

class ControladorTipo{
    


 function __constructor() { 
		  
    }
    
 public function insertarTipo($tipo){


  
     $tipoModel= new Tipo();
     $id =$tipoModel->insert($tipo);

    
     $v= ($id>0);
      $respuesta = new Respuesta( $v ? EMensaje::INSERCION_EXITOSA : EMensaje::ERROR_INSERSION);
      $respuesta->setDatos($id);
      return $respuesta;

  
   
    }
     public function listarTipo(){
        $tipoModel= new Tipo();
        $tipoModel->where("estado","=","1");
        $lista=$tipoModel->get("*");
        $v= (count($lista));
        $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
        $respuesta->setDatos($lista);
        return $respuesta;

     }

     public function listarPag($cantidad, $pagina, $orden, $dir){
      $tipoModel= new Tipo();
      $lista=$tipoModel->getPag("*",$cantidad, $pagina, $orden, $dir);
      $v= (count($lista));
      $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
      $respuesta->setDatos($lista);
      return $respuesta;
   }

   public function listarReg(){
      $tipoModel= new Tipo();
      $lista=$tipoModel->get("*");
      $v= (count($lista));
      $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
      $respuesta->setDatos($v);
      return $respuesta;
   }




     public function actualizar($tipo){
        $tipoModel= new tipo();
        $actualizados =$tipoModel->where("idTipo","=", $tipo["idTipo"])
        ->update($tipo);
        $v= ($actualizados>0);
        $respuesta = new Respuesta( $v ? EMensaje::ACTUALIZACION_EXITOSA : EMensaje::ERROR_ACTUALIZACION);
        $respuesta->setDatos($actualizados);
        return $respuesta;
     }

     

     public function buscarTipo ($nombre)
     {
        $tipoModel= new Tipo();
        $tipoModel->where("nombre","LIKE",$nombre.'%');
        $tipo=$tipoModel->get("idTipo,nombre");
        $v= ($tipo);
        $respuesta = new Respuesta( $v ? EMensaje::CORREO_CORRECTO : EMensaje::ERROR_CORREO);
        $respuesta->setDatos($tipo);
        return $respuesta;
     }



     public function nombreTipo ($id)
     {
        $tipoModel= new Tipo();
        $tipoModel->where("idTipo","=",$id);
        $tipo=$tipoModel->first("nombre");
        $v= ($tipo);
        $respuesta = new Respuesta( $v ? EMensaje::CORREO_CORRECTO : EMensaje::ERROR_CORREO);
        $respuesta->setDatos($tipo);
        return $respuesta;
     }

 }

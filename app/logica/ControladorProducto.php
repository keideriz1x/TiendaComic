<?php

class ControladorProducto{
    


 function __constructor() { 
		  
    }
    
 public function insertarProducto($producto){


  
     $productoModel= new Producto();
     $id =$productoModel->insert($producto);

    
     $v= ($id>0);
      $respuesta = new Respuesta( $v ? EMensaje::INSERCION_EXITOSA : EMensaje::ERROR_INSERSION);
      $respuesta->setDatos($id);
      return $respuesta;

  
   
    }
     public function listarProducto(){
        $productoModel= new Producto();
        $actualizados =$productoModel->where("stock",">", "0");
        $lista=$productoModel->get("idProducto,nombre,precio,stock,Tipo_idTipo,foto,Categoria_idCategoria,descripcion");
        
        $v= (count($lista));
        $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
        $respuesta->setDatos($lista);
        return $respuesta;

     }

     public function listarPag($cantidad, $pagina, $orden, $dir){
      $productoModel= new Producto();
      $lista=$productoModel->getPag("idProducto,nombre,precio,stock,Tipo_idTipo,foto,Categoria_idCategoria,descripcion",$cantidad, $pagina, $orden, $dir);
      
      $v= (count($lista));
      $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
      $respuesta->setDatos($lista);
      return $respuesta;

   }

   public function ListarReg(){
      $productoModel= new Producto();
      $lista=$productoModel->get("idProducto");
      
      $v= (count($lista));
      $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
      $respuesta->setDatos($v);
      return $respuesta;

   }

     public function actualizar($producto){
        $productoModel= new Producto();
        $actualizados =$productoModel->where("idProducto","=", $producto["idProducto"])
        ->update($producto);
        $v= ($actualizados>0);
        $respuesta = new Respuesta( $v ? EMensaje::ACTUALIZACION_EXITOSA : EMensaje::ERROR_ACTUALIZACION);
        $respuesta->setDatos($actualizados);
        return $respuesta;
     }

     

     public function buscarProducto($nombre)
     {
        $productoModel= new Producto();
        $productoModel->where("nombre","LIKE",$nombre.'%');
        $producto=$productoModel->get("idProducto,nombre,precio,stock,Categoria_idCategoria,Tipo_idTipo,foto");
        $v= ($producto);
        $respuesta = new Respuesta( $v ? EMensaje::CORREO_CORRECTO : EMensaje::ERROR_CORREO);
        $respuesta->setDatos($producto);
        return $respuesta;
     }


     
     public function cargarInformacion ($id)
     {
        $productoModel= new Producto();
        $productoModel->where("idProducto","=",$id);
        $producto=$productoModel->first("idProducto, nombre,precio,stock,Categoria_idCategoria,Tipo_idTipo,foto,descripcion");
        $v= ($producto);
        $respuesta = new Respuesta( $v ? EMensaje::CORREO_CORRECTO : EMensaje::ERROR_CORREO);
        $respuesta->setDatos($producto);
        return $respuesta;
     }


     public function stock ($id)
     {
        $productoModel= new Producto();
        $productoModel->where("idProducto","=",$id);
        $producto=$productoModel->first("stock");
        $v= ($producto);
        $respuesta = new Respuesta( $v ? EMensaje::CORREO_CORRECTO : EMensaje::ERROR_CORREO);
        $respuesta->setDatos($producto);
        return $respuesta;
     }



     



     public function buscarProductoF ($nombre,$cantidad, $pagina, $orden, $dir)
     {
        $productoModel= new Producto();
        $productoModel->where("nombre","LIKE",$nombre.'%');
        $producto=$productoModel->getPag("idProducto,nombre,precio,stock,Categoria_idCategoria,Tipo_idTipo,foto",$cantidad, $pagina, $orden, $dir);
        $v= ($producto);
        $respuesta = new Respuesta( $v ? EMensaje::CORREO_CORRECTO : EMensaje::ERROR_CORREO);
        $respuesta->setDatos($producto);
        return $respuesta;
     }





 }

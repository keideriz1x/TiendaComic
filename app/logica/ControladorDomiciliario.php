<?php

class ControladorDomiciliario{

    function __constructor() { 
		  
    }
    
 public function insertarDomiciliario($domiciliario){


  
     $domiciliarioModel= new Domiciliario();
     $id =$domiciliarioModel->insert($domiciliario);

    
     $v= ($id>0);
      $respuesta = new Respuesta( $v ? EMensaje::INSERCION_EXITOSA : EMensaje::ERROR_INSERSION);
      $respuesta->setDatos($id);
      return $respuesta;

  
   
    }
     public function listarDomiciliario(){
        $domiciliarioModel= new Domiciliario();
        $lista=$domiciliarioModel->get("idDomiciliario,nombre,apellido,correo,estado");
        $v= (count($lista));
        $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
        $respuesta->setDatos($lista);
        return $respuesta;

     }

     public function listarPag($cantidad, $pagina, $orden, $dir){
      $domiciliarioModel= new Domiciliario();
      $lista=$domiciliarioModel->getPag("idDomiciliario,nombre,apellido,correo,estado",$cantidad, $pagina, $orden, $dir);
      $v= (count($lista));
      $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
      $respuesta->setDatos($lista);
      return $respuesta;
   }


   public function listarReg(){
      $domiciliarioModel= new Domiciliario();
      $lista=$domiciliarioModel->get("idDomiciliario");
      $v= (count($lista));
      $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
      $respuesta->setDatos($v);
      return $respuesta;

   }



     public function actualizar($domiciliario){
        $domiciliarioModel= new Domiciliario();
        $actualizados =$domiciliarioModel->where("idDomiciliario","=", $domiciliario["idDomiciliario"])
        ->update($domiciliario);
        $v= ($actualizados>0);
        $respuesta = new Respuesta( $v ? EMensaje::ACTUALIZACION_EXITOSA : EMensaje::ERROR_ACTUALIZACION);
        $respuesta->setDatos($actualizados);
        return $respuesta;
     }

     public function loginDomiciliario ($correo )
     {
        $domiciliarioModel= new Domiciliario();
        $domiciliarioModel->where("correo","=",$correo);
      
        $domiciliario=$domiciliarioModel->first("idDomiciliario,nombre,apellido,foto,estado,clave");
       
        $v= ($domiciliario);
        $respuesta = new Respuesta( $v ? EMensaje::LOGIN_EXITOSO : EMensaje::ERROR_LOGIN);
        $respuesta->setDatos($domiciliario);
        return $respuesta;
     }

     public function buscarCorreo ($correo)
     {
        $domiciliarioModel= new Domiciliario();
        $domiciliarioModel->where("correo","=",$correo);
        $domiciliario=$domiciliarioModel->first("correo");
        $v= ($domiciliario);
        $respuesta = new Respuesta( $v ? EMensaje::CORREO_CORRECTO : EMensaje::ERROR_CORREO);
        $respuesta->setDatos($domiciliario);
        return $respuesta;
     }



     public function correoExistente($correo)
     {
        $domiciliarioModel= new Domiciliario();
        $domiciliarioModel->where("correo","=",$correo);
        $domiciliario=$domiciliarioModel->first("correo");
        $v= ($domiciliario);
        $respuesta = new Respuesta( $v ? EMensaje::CORREO_CORRECTO : EMensaje::ERROR_CORREO);
         $respuesta->setDatos($domiciliario);
        return $respuesta;
     }



     public function buscarDomiciliario ($dato)
     {
        $domiciliarioModel= new Domiciliario();
        $domiciliarioModel->where("nombre","LIKE",$dato.'%');
        $domiciliarioModel->orWhere("apellido","LIKE",$dato.'%');
        $domiciliario=$domiciliarioModel->get("idDomiciliario,nombre,apellido,correo,estado");
        $v= ($domiciliario);
        $respuesta = new Respuesta( $v ? EMensaje::CORREO_CORRECTO : EMensaje::ERROR_CORREO);
        $respuesta->setDatos($domiciliario);
        return $respuesta;
     }


     
     public function cargarInformacion ($id )
     {
        $domiciliarioModel= new Domiciliario();
        $domiciliarioModel->where("idDomiciliario","=",$id);
        
        $domiciliario=$domiciliarioModel->first("nombre,apellido,correo,foto");
       
        $v= ($domiciliario);
        $respuesta = new Respuesta( $v ? EMensaje::LOGIN_EXITOSO : EMensaje::ERROR_LOGIN);
        $respuesta->setDatos($domiciliario);
        return $respuesta;
     }


     public function verificarClave ($id)
     {
        $domiciliarioModel= new Domiciliario();
        $domiciliarioModel->where("idDomiciliario","=",$id);
        $domiciliario=$domiciliarioModel->first("clave");
        $v= ($domiciliario);
        $respuesta = new Respuesta( $v ? EMensaje::CORREO_CORRECTO : EMensaje::ERROR_CORREO);
        $respuesta->setDatos($domiciliario);
        return $respuesta;
     }


     public function foto ($id )
     {
        $domiciliarioModel= new Domiciliario();
        $domiciliarioModel->where("idDomiciliario","=",$id);
        
        $domiciliario=$domiciliarioModel->first("foto");
       
        $v= ($domiciliario);
        $respuesta = new Respuesta( $v ? EMensaje::LOGIN_EXITOSO : EMensaje::ERROR_LOGIN);
        $respuesta->setDatos($domiciliario);
        return $respuesta;
     }

}

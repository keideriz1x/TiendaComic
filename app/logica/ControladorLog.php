<?php

class ControladorLog{

    function __constructor(){}

    public function insertarRegistro($registro){
        $logModel= new Log();
        $id=$logModel->insert($registro);
        $v= ($id>0);
        $respuesta = new Respuesta( $v ? EMensaje::INSERCION_EXITOSA : EMensaje::ERROR_INSERSION);
        $respuesta->setDatos($id);
        return $respuesta;
    }

    public function listarRegistro(){
        $logModel= new Log();
        $lista=$logModel->get("*");
        $v= (count($lista));
        $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
        $respuesta->setDatos($lista);
        return $respuesta;
     }

     public function buscarRegistro($registro)
     {
        $logModel= new Log();
        $logModel->where("accion","LIKE",$registro.'%');
        $logModel->orWhere("actor","LIKE",$registro.'%');
        $logModel->orWhere("fechayhora","LIKE",'%'.$registro.'%');
        $producto=$logModel->get("idLog,accion,fechayhora,actor,Administrador_idAdministrador,descripcion");
        $v= ($producto);
        $respuesta = new Respuesta( $v ? EMensaje::CORREO_CORRECTO : EMensaje::ERROR_CORREO);
        $respuesta->setDatos($producto);
        return $respuesta;
     }



     public function listarPag($cantidad, $pagina, $orden, $dir){
        $logModel= new Log();
        $lista=$logModel->getPag("idLog,accion,fechayhora,actor,Administrador_idAdministrador,descripcion",$cantidad, $pagina, $orden, $dir);
        $v= (count($lista));
        $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
        $respuesta->setDatos($lista);
        return $respuesta;
     }
  
     public function listarReg(){
        $logModel= new Log();
        $lista=$logModel->get("idLog");
        $v= (count($lista));
        $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
        $respuesta->setDatos($v);
        return $respuesta;
     }

}
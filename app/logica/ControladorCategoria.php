<?php

class ControladorCategoria{
    


 function __constructor() { 
		  
    }
    
 public function insertarCategoria($categoria){


  
     $categoriaModel= new Categoria();
     $id =$categoriaModel->insert($categoria);

    
     $v= ($id>0);
      $respuesta = new Respuesta( $v ? EMensaje::INSERCION_EXITOSA : EMensaje::ERROR_INSERSION);
      $respuesta->setDatos($id);
      return $respuesta;

  
   
    }
     public function listarCategoria(){
        $categoriaModel= new Categoria();
        $categoriaModel->where("estado","=","1");

        $lista=$categoriaModel->get("*");
        $v= (count($lista));
        $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
        $respuesta->setDatos($lista);
        return $respuesta;

     }
  
     public function listarPag($cantidad, $pagina, $orden, $dir){
      $categoriaModel= new Categoria();
      $lista=$categoriaModel->getPag("*",$cantidad, $pagina, $orden, $dir);
      $v= (count($lista));
      $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
      $respuesta->setDatos($lista);
      return $respuesta;
   }

   public function listarReg(){
      $categoriaModel= new Categoria();
      $lista=$categoriaModel->get("idCategoria");
      $v= (count($lista));
      $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
      $respuesta->setDatos($v);
      return $respuesta;
   }

     public function actualizar($categoria){
        $categoriaModel= new Categoria();
        $actualizados =$categoriaModel->where("idCategoria","=", $categoria["idCategoria"])
        ->update($categoria);
        $v= ($actualizados>0);
        $respuesta = new Respuesta( $v ? EMensaje::ACTUALIZACION_EXITOSA : EMensaje::ERROR_ACTUALIZACION);
        $respuesta->setDatos($actualizados);
        return $respuesta;
     }

     

     public function buscarCategoria ($nombre)
     {
        $categoriaModel= new Categoria();
        $categoriaModel->where("nombre","LIKE",$nombre.'%');
        $categoria=$categoriaModel->get("idCategoria,nombre");
        $v= ($categoria);
        $respuesta = new Respuesta( $v ? EMensaje::CORREO_CORRECTO : EMensaje::ERROR_CORREO);
        $respuesta->setDatos($categoria);
        return $respuesta;
     }



     public function nombreCategoria($id)
     {
        $categoriaModel= new Categoria();
        $categoriaModel->where("idCategoria","=",$id);
        $categoria=$categoriaModel->first("nombre");
        $v= ($categoria);
        $respuesta = new Respuesta( $v ? EMensaje::CORREO_CORRECTO : EMensaje::ERROR_CORREO);
        $respuesta->setDatos($categoria);
        return $respuesta;
     }

 }

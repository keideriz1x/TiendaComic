<?php

class ControladorAdministrador{
    


 function __constructor() { 
		  
    }
    
 public function insertarAdministrador($administrador){


  
     $administradorModel= new Administrador();
     $id =$administradorModel->insert($administrador);

    
     $v= ($id>0);
      $respuesta = new Respuesta( $v ? EMensaje::INSERCION_EXITOSA : EMensaje::ERROR_INSERSION);
      $respuesta->setDatos($id);
      return $respuesta;

  
   
    }
     public function listarAdministrador(){
        $administradorModel= new Administrador();
        $lista=$administradorModel->get("*");
        $v= (count($lista));
        $respuesta = new Respuesta( $v ? EMensaje::CORRECTO : EMensaje::ERROR);
        $respuesta->setDatos($lista);
        return $respuesta;

     }

     public function actualizar($administrador){
        $administradorModel= new Administrador();
        $actualizados =$administradorModel->where("idAdministrador","=", $administrador["idAdministrador"])
        ->update($administrador);
        $v= ($actualizados>0);
        $respuesta = new Respuesta( $v ? EMensaje::ACTUALIZACION_EXITOSA : EMensaje::ERROR_ACTUALIZACION);
        $respuesta->setDatos($actualizados);
        return $respuesta;
     }

     public function loginAdministrador ($correo )
     {
        $administradorModel= new Administrador();
        $administradorModel->where("correo","=",$correo);
        
        $administrador=$administradorModel->first("idAdministrador,nombre,apellido,clave,foto");
       
        $v= ($administrador);
        $respuesta = new Respuesta( $v ? EMensaje::LOGIN_EXITOSO : EMensaje::ERROR_LOGIN);
        $respuesta->setDatos($administrador);
        return $respuesta;
     }

     public function buscarCorreo ($correo)
     {
        $administradorModel= new Administrador();
        $administradorModel->where("correo","=",$correo);
        $administrador=$administradorModel->first("correo");
        $v= ($administrador);
        $respuesta = new Respuesta( $v ? EMensaje::CORREO_CORRECTO : EMensaje::ERROR_CORREO);
        $respuesta->setDatos($administrador);
        return $respuesta;
     }


     public function verificarClave ($id)
     {
        $administradorModel= new Administrador();
        $administradorModel->where("idAdministrador","=",$id);
        $administrador=$administradorModel->first("clave");
        $v= ($administrador);
        $respuesta = new Respuesta( $v ? EMensaje::CORREO_CORRECTO : EMensaje::ERROR_CORREO);
        $respuesta->setDatos($administrador);
        return $respuesta;
     }


     public function cargarInformacion ($id )
     {
        $administradorModel= new Administrador();
        $administradorModel->where("idAdministrador","=",$id);
        
        $administrador=$administradorModel->first("nombre,apellido,correo,foto");
       
        $v= ($administrador);
        $respuesta = new Respuesta( $v ? EMensaje::LOGIN_EXITOSO : EMensaje::ERROR_LOGIN);
        $respuesta->setDatos($administrador);
        return $respuesta;
     }


     public function cargarNombre ($id )
     {
        $administradorModel= new Administrador();
        $administradorModel->where("idAdministrador","=",$id);
        
        $administrador=$administradorModel->first("nombre,apellido");
       
        $v= ($administrador);
        $respuesta = new Respuesta( $v ? EMensaje::LOGIN_EXITOSO : EMensaje::ERROR_LOGIN);
        $respuesta->setDatos($administrador);
        return $respuesta;
     }

     public function foto ($id )
     {
        $administradorModel= new Administrador();
        $administradorModel->where("idAdministrador","=",$id);
        
        $administrador=$administradorModel->first("foto");
       
        $v= ($administrador);
        $respuesta = new Respuesta( $v ? EMensaje::LOGIN_EXITOSO : EMensaje::ERROR_LOGIN);
        $respuesta->setDatos($administrador);
        return $respuesta;
     }

 }


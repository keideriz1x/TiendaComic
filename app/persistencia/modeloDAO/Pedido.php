<?php

class Pedido extends ModeloGenerico{
    protected $idPedido;
    protected $carrito;
	protected $Domiciliario_idDomiciliario;
	protected $estado;

    public function __construct($propiedades=null){
        parent:: __construct("pedido",Pedido::class. $propiedades);
    }
    


	function getIdPedido() { 
 		return $this->idPedido; 
	} 

	function setIdPedido($idPedido) {  
		$this->idPedido = $idPedido; 
	} 

	function getCarrito() { 
 		return $this->carrito; 
	} 

	function setCarrito($carrito) {  
		$this->carrito = $carrito; 
	} 

	function getDomiciliarioidDomiciliario() { 
 		return $this->Domiciliario_idDomiciliario; 
	} 

	function setDomiciliarioidDomiciliario($DomiciliarioidDomiciliario) {  
		$this->Domiciliario_idDomiciliario = $DomiciliarioidDomiciliario; 
	} 
}
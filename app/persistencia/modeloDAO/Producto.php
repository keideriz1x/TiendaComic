<?php

class Producto extends ModeloGenerico{
    protected $idProducto;
    protected $nombre;
    protected $precio;
    protected $stock;
    protected $foto;
    protected $Categoria_idCategoria;
    protected $Tipo_idTipo;
    protected $descripcion;

    public function __construct($propiedades=null){
        parent:: __construct("producto",Producto::class. $propiedades);
    }

	function getIdProducto() { 
 		return $this->idProducto; 
	} 

	function setIdProducto($idProducto) {  
		$this->idProducto = $idProducto; 
	} 

	function getNombre() { 
 		return $this->nombre; 
	} 

	function setNombre($nombre) {  
		$this->nombre = $nombre; 
	} 

	function getPrecio() { 
 		return $this->precio; 
	} 

	function setPrecio($precio) {  
		$this->precio = $precio; 
	} 

	function getStock() { 
 		return $this->stock; 
	} 

	function setStock($stock) {  
		$this->stock = $stock; 
	} 

	function getFoto() { 
 		return $this->foto; 
	} 

	function setFoto($foto) {  
		$this->foto = $foto; 
	} 

	function getCategoriaidCategoria() { 
 		return $this->Categoria_idCategoria; 
	} 

	function setCategoriaidCategoria($CategoriaidCategoria) {  
		$this->Categoria_idCategoria = $CategoriaidCategoria; 
	} 

	function getTipoidTipo() { 
 		return $this->Tipo_idTipo; 
	} 

	function setTipoidTipo($TipoidTipo) {  
		$this->Tipo_idTipo = $TipoidTipo; 
	} 

	function getDescripcion() { 
 		return $this->descripcion; 
	} 

	function setDescripcion($descripcion) {  
		$this->descripcion = $descripcion; 
	} 
}
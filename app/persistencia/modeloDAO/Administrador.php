<?php

Class Administrador extends ModeloGenerico{
    protected $idAdministrador;
    protected $nombre;
    protected $apellido;
    protected $correo;
    protected $clave;
    protected $foto;

    public function __construct($propiedades=null){
        parent:: __construct("administrador",Administrador::class. $propiedades);
    }



    //getters

	function getIdAdministrador() { 
 		return $this->idAdministrador; 
	} 

	function getNombre() { 
 		return $this->nombre; 
	} 

	function getApellido() { 
 		return $this->apellido; 
	} 

	function getCorreo() { 
 		return $this->correo; 
	} 

	function getClave() { 
 		return $this->clave; 
	} 

	function getFoto() { 
 		return $this->foto; 
    } 
    
    //setters

    

	function setIdAdministrador($idAdministrador) {  
		$this->idAdministrador = $idAdministrador; 
	} 

	function setNombre($nombre) {  
		$this->nombre = $nombre; 
	} 

	function setApellido($apellido) {  
		$this->apellido = $apellido; 
	} 

	function setCorreo($correo) {  
		$this->correo = $correo; 
	} 

	function setClave($clave) {  
		$this->clave = $clave; 
	} 

	function setFoto($foto) {  
		$this->foto = $foto; 
	} 
}
<?php

    class Carrito extends ModeloGenerico{
        protected $idCarrito;
        protected $estado;
		protected $Cliente_idCliente;
		protected $fechaCompra;

        public function __construct($propiedades=null){
            parent:: __construct("carrito",Carrito::class. $propiedades);
        }


        

	function getIdCarrito() { 
 		return $this->idCarrito; 
	} 

	function setIdCarrito($idCarrito) {  
		$this->idCarrito = $idCarrito; 
	} 

	function getEstado() { 
 		return $this->estado; 
	} 

	function setEstado($estado) {  
		$this->estado = $estado; 
	} 

	function getClienteidCliente() { 
 		return $this->Cliente_idCliente; 
	} 

	function setClienteidCliente($ClienteidCliente) {  
		$this->Cliente_idCliente = $ClienteidCliente; 
	} 
    }
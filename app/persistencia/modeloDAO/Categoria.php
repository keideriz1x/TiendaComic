<?php

Class Categoria extends ModeloGenerico{
    protected $idCategoria;
    protected $nombre;
    protected $estado;


    public function __construct($propiedades=null){
        parent:: __construct("categoria",Categoria::class. $propiedades);
    }

    function getIdCategoria(){
        return $this->IdCategoria;
    }
    function getNombre(){
        return $this->nombre;
    }
    function setIdCategoria ($id)
    {
        $this->idCategoria=$id;
    }
    function setNombre($nombre){
        $this->nombre=$nombre;
    }
}
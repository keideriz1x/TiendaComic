<?php

class Log extends ModeloGenerico{
    protected $idLog;
    protected $accion;
    protected $fechayhora;
    protected $actor;
    protected $ip;
    protected $SO;
	protected $Administrador_idAdministrador;
    protected $descripcion;
	

    public function __construct($propiedades=null){
        parent:: __construct("log",Log::class. $propiedades);
    }

    

	function getIdlog() { 
 		return $this->idLog; 
	} 

	function setIdlog($idlog) {  
		$this->idLog = $idlog; 
	} 

	function getAccion() { 
 		return $this->accion; 
	} 

	function setAccion($accion) {  
		$this->accion = $accion; 
	} 

	function getFechayhora() { 
 		return $this->fechayhora; 
	} 

	function setFechayhora($fechayhora) {  
		$this->fechayhora = $fechayhora; 
    } 
    

	function getActor() { 
 		return $this->actor; 
	} 

	function setActor($actor) {  
		$this->actor = $actor; 
	} 

	function getIp() { 
 		return $this->ip; 
	} 

	function setIp($ip) {  
		$this->ip = $ip; 
	} 

	function getSO() { 
 		return $this->SO; 
	} 

	function setSO($SO) {  
		$this->SO = $SO; 
	} 

	function getAdministradoridAdministrador() { 
 		return $this->Administrador_idAdministrador; 
	} 

	function setAdministradoridAdministrador($AdministradoridAdministrador) {  
		$this->Administrador_idAdministrador = $AdministradoridAdministrador; 
	} 
}
<?php

class Producto_has_carrito extends ModeloGenerico{

    protected $Producto_idProducto;
    protected $Carrito_idCarrito;
    protected $cantidad;

		

    public function __construct($propiedades=null){
        parent:: __construct("producto_has_carrito",Producto_has_carrito::class. $propiedades);
    }

    

	function getProductoidProducto() { 
 		return $this->Producto_idProducto; 
	} 

	function setProductoidProducto($ProductoidProducto) {  
		$this->Producto_idProducto = $ProductoidProducto; 
	} 

	function getCarritoidCarrito() { 
 		return $this->Carrito_idCarrito; 
	} 

	function setCarritoidCarrito($CarritoidCarrito) {  
		$this->Carrito_idCarrito = $CarritoidCarrito; 
	} 

	function getCantidad() { 
 		return $this->cantidad; 
	} 

	function setCantidad($cantidad) {  
		$this->cantidad = $cantidad; 
	} 
}
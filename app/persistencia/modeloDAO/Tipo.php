<?php

class Tipo extends ModeloGenerico{
    protected $idTipo;   
	protected $nombre;
    protected $estado;
	

    public function __construct($propiedades=null){
        parent:: __construct("tipo",Tipo::class. $propiedades);
    }

    


	function getIdTipo() { 
 		return $this->idTipo; 
	} 

	function setIdTipo($idTipo) {  
		$this->idTipo = $idTipo; 
	} 

	function getNombre() { 
 		return $this->nombre; 
	} 

	function setNombre($nombre) {  
		$this->nombre = $nombre; 
	} 
}
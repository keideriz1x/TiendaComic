<?php
include "app/presentacion/menuinicio.php";
?>

<script src="app/presentacion/producto/tienda.js"></script>


<div class="col-11 container">
    <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
        </ol>
        <div class="contentCarrusel carousel-inner">
            <div class="carousel-item active">
                <img src="app/img/img1.jpg" class="carrusel d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                </div>
            </div>
            <div class="carousel-item">
                <img src="app/img/img2.jpg" class="carrusel d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                </div>
            </div>
            <div class="carousel-item">
                <img src="app/img/img3.jpg" class="carrusel d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                </div>
            </div>
            <div class="carousel-item">
                <img src="app/img/img4.jpg" class="carrusel d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

<div class="col-11 container pt-3">
    <div class="col-xs-12 pt-2">
        <div class="input-group">
            <div class="input-group-prepend my-auto">
                <h5>JD Comics</h5>
            </div>
            <div class="form-group col">
                <hr>
            </div>
            <span class="input-group-prepend">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class='pag page-item pr-2'><a class='page-link' href='#' aria-label='Previous'><span aria-hidden='true'>&laquo;</span></a></li>

                        <li class='pag page-item pl-2'><a class='page-link' href='#'><span aria-hidden='true'>&raquo;</span></a></li>
                    </ul>
                </nav>
            </span>
        </div>
    </div>
</div>
<div class="col-11 container pt-3">
    <div class="row">
        <?php



$cantidad = 12;
if(isset($_GET["cantidad"])){
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if(isset($_GET["pagina"])){
    $pagina = $_GET["pagina"];
}
$orden = "";
if(isset($_GET["orden"])){
    $orden = $_GET["orden"];
}
$dir = "";
if(isset($_GET["dir"])){
    $dir = $_GET["dir"];
}

 $data = new ControladorProducto();


 $totalRegistros = $data ->ListarReg();
$totalPaginas = intval(($totalRegistros->getDatos()/$cantidad));
 if($totalRegistros->getDatos()%$cantidad != 0){
     $totalPaginas++;
 }
   
        $id = $data->listarProducto();
        $tipo = $id->getDatos();
        $categoria = new ControladorCategoria();

        $type = new ControladorTipo();
        
        foreach ($tipo as $clave => $valor) {
            $valor->foto = base64_encode($valor->foto);

            $index = intval($valor->Categoria_idCategoria);
            $data = $categoria->nombreCategoria($index);
            $valor->Categoria_idCategoria = ($data->getDatos())->nombre;

            $index = intval($valor->Tipo_idTipo);
            $data = $type->nombreTipo($index);
            $valor->Tipo_idTipo = ($data->getDatos())->nombre;


        ?>

            <div class="capa col-xl-3 col-md-6 pt-3 border-left border-right">
                <div class="thumbnail-container">
                    <a href="?" data-toggle="modal" data-target="#ver<?php echo $valor->idProducto ?>">
                        <img src="data:image/png;base64,<?php echo $valor->foto ?>" class="imageninicio zoom mx-auto d-block">
                    </a>
                </div>
                <div class="product__info">
                    <div class="product-group">
                        <div class="product__title">
                            <a href="?" data-toggle="modal" data-target="#ver<?php echo $valor->idProducto ?>"><?php echo $valor->nombre ?></a>
                        </div>
                        <div class="pt-2">
                            <p> Categoria <?php echo $valor->Categoria_idCategoria ?> </p>
                            <span>
                                <img class="banderainicio mb-1" src="https://img.icons8.com/emoji/48/000000/colombia-emoji.png" /><span>$ <?php echo $valor->precio ?> COP</span>
                            </span>
                            <br>
                            <?php
                            if ($valor->stock < 10) {
                            ?>
                                <p> solo queda(n) : <?php echo $valor->stock ?> unidades</p>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="group-buttons d-flex">
                        <div class="mostrar">
                            <button type="button" class="btn screen-reader-text text-white px-2  btn-lg " data-toggle="modal" data-target="#id<?php echo $valor->idProducto ?>">                       
                                <i class="fas fa-shopping-cart"></i>
                            </button>
                        </div>

                        <div class=" contenedor modal fade pt-5" id="id<?php echo $valor->idProducto ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <span id="<?php echo $valor->idProducto ?>"> </span>
                                    <div class="modal-header">
                                        <button type="button" class="close popupCartClose" data-dismiss="modal" aria-label="Close"><i class="zmdi zmdi-hc-fw zmdi-close"></i></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                <img src="data:image/png;base64,<?php echo $valor->foto ?>" class="imgcarritoinicio mx-auto d-block">
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8">
                                                <div class="mb-5"><?php echo $valor->nombre ?></div>
                                                <p class="mb-10">En Carrito</p>
                                                <div class="d-flex">
                                                    <a href="#" data-dismiss="modal" class="btn btn-primary mr-2 carrito" >
                                                        <input class="id"  type="hidden" value="<?php echo $valor->idProducto?>"> </input>
                                                     <input class="nombre"  type="hidden" value="<?php echo $valor->nombre?>"> </input>
                                                     <input class="precio"  type="hidden" value="<?php echo $valor->precio?>"> </input>
                                                     <input class="foto"  type="hidden" value="<?php echo $valor->foto?>"> </input>                                                                                     
                                                    Continuar</a>
                                                    <a href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoCliente.php") ?>&priv=<?php echo base64_encode(base64_encode("carrito")) ?>" class="btn btn-danger carrito "> 
                                                    <input class="id"  type="hidden" value="<?php echo $valor->idProducto?>"> </input>
                                                     <input class="nombre"  type="hidden" value="<?php echo $valor->nombre?>"> </input>
                                                     <input class="precio"  type="hidden" value="<?php echo $valor->precio?>"> </input>
                                                     <input class="foto"  type="hidden" value="<?php echo $valor->foto?>"> </input>                                                           
                                                    Ir a Carrito</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        <div class="mostrar pl-2">
                            <button type="button" class="btn screen-reader-text text-white px-2  btn-lg " data-toggle="modal" data-target="#ver<?php echo $valor->idProducto ?>">

                                <i class="fa fa-search text-white"></i>
                            </button>
                        </div>
                        <div class="contenedor modal fade pt-5" id="ver<?php echo $valor->idProducto ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" role="dialog">
                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close quickviewClose" data-dismiss="modal" aria-label="Close"><i class="zmdi zmdi-hc-fw zmdi-close"></i></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="proBoxPrimary row spacing-15">
                                            <div class="proBoxImage col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                                <img src="data:image/png;base64,<?php echo $valor->foto ?>" class="imgmodalinicio mx-auto d-block">
                                            </div>
                                            <div class="proBoxInfo col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                <div>
                                                    <h1><?php echo $valor->nombre ?></h1>
                                                </div>
                                                <div>Disponibilidad: En Stock</div>
                                                <div>Vendido por: JD Comics</div>
                                                <div>Tipo: <?php echo $valor->Tipo_idTipo ?> </div>
                                                <div>Categoria: <?php echo $valor->Categoria_idCategoria ?></div>
                                                <div>Descripcion:
                                                    <button data-toggle="modal" data-target="#des<?php echo $valor->idProducto ?>" class="btn"><i class="far fa-eye"></i></button>
                                                    
                                                    <div class="modal" id="des<?php echo $valor->idProducto ?>" data-backdrop="static">
                                                        <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title"><?php echo $valor->nombre ?></h4>
                                                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                                                </div>
                                                                <div class="container"></div>
                                                                <div class="modal-body contenedor">
                                                                    <?php  echo $valor->descripcion?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <form method="post" enctype="multipart/form-data">
                                                    <div>
                                                        <img class="mb-2" src="https://img.icons8.com/emoji/48/000000/colombia-emoji.png" /><span style="font-size: 24px;">$ <?php echo $valor->precio ?> COP</span>
                                                    </div>
                                                    <div class="d-flex mt-30">
                                                        <div>
                                                            <a href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoCliente.php") ?>&priv=<?php echo base64_encode(base64_encode("carrito")) ?>" style='text-decoration:none' class="btn text-white screen-reader-text carrito">
                                                                <input class="id"  type="hidden" value="<?php echo $valor->idProducto?>"> </input>
                                                                 <input class="nombre"  type="hidden" value="<?php echo $valor->nombre?>"> </input>
                                                                <input class="precio"  type="hidden" value="<?php echo $valor->precio?>"> </input>
                                                                <input class="foto"  type="hidden" value="<?php echo $valor->foto?>"> </input>
                                                                <i class="fas fa-shopping-cart mt-2"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="id" value="38038138421446">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>


    </div>

    <div class="row pt-3">
					<div class="col-md-10 mr-auto">
						<nav aria-label="Page navigation example">
						<?php
										// calculamos la primera y última página a mostrar
							$primera = $pagina - ($pagina % 5) + 1;
							if ($primera > $pagina) {
								$primera = $primera - 5;
							}
							$ultima = $primera + 4 > $totalPaginas ? $totalPaginas : $primera + 4;
							?>
								<ul class="pagination pagination-sm" >
									<?php
									if ($totalPaginas > 1) {
										// comprobamos $primera en lugar de $pagina
										if ($pagina==1) {
											echo "<li class='pag page-item disabled'><a class='page-link' href='#' aria-label='Previous'><span aria-hidden='true'>&laquo;</span></a></li>";
										} elseif($pagina>1){
											echo "<li class='pag page-item'><a class='page-link' href='index.php?pid=" . base64_encode("app/presentacion/inicio.php")   . "&pagina=" . ($pagina - 1) . "&cantidad=" . $cantidad . (($orden != "") ? "&orden=" . $orden : "") . (($dir != "") ? "&dir=" . $dir : "") . "' aria-label='Previous'><span aria-hidden='true'>&laquo;</span></a></li>";
										}
										// mostramos de la primera a la última
										for ($i = $primera; $i <= $ultima; $i++) {
											if ($pagina == $i)
												echo "<li class='pag page-item active'><a class='page-link' href='index.php?pid=" . base64_encode("app/presentacion/inicio.php")  . "&pagina=" . $i . "&cantidad=" . $cantidad . (($orden != "") ? "&orden=" . $orden : "") . (($dir != "") ? "&dir=" . $dir : "") . "'>" . $i . "</a></li>";
											else
												echo "<li class='pag page-item'><a class='page-link' href='index.php?pid=" . base64_encode("app/presentacion/inicio.php")   . "&pagina=" . $i . "&cantidad=" . $cantidad . (($orden != "") ? "&orden=" . $orden : "") . (($dir != "") ? "&dir=" . $dir : "") . "'>" . $i . "</a></li>";
										}

										if ($i > $pagina && $pagina==$totalPaginas) {
											echo "<li class='pag page-item disabled'><a class='page-link' href='#'><span aria-hidden='true'>&raquo;</span></a></li>";
										}else{
											echo "<li class='pag page-item'><a class='page-link' href='index.php?pid=" . base64_encode("app/presentacion/inicio.php")   . "&pagina=" . ($pagina + 1) . "&cantidad=" . $cantidad . (($orden != "") ? "&orden=" . $orden : "") . (($dir != "") ? "&dir=" . $dir : "") . "'><span aria-hidden='true'>&raquo;</span></a></li>";
										}
									}
									?>
                                    </ul>	
						</nav>
					</div>
					<div class="col-md-2 text-right" >
						<select name="cantidad" id="cantidad" class="form-control form-control-sm">
						<option value="12" <?php echo ($cantidad==12)?"selected":"" ?>>12</option>
								<option value="24" <?php echo ($cantidad==24)?"selected":"" ?>>24</option>
								<option value="48" <?php echo ($cantidad==48)?"selected":"" ?>>48</option>
						</select>
						<input   id="pagina" type="hidden" value ="<?php echo $pagina  ?>"></input>
					</div>
				</div>
</div>

<script>
$("#cantidad").on("change", function() {
	url = "index.php?pid=<?php echo base64_encode("app/presentacion/inicio.php") ?>&priv=<?php echo base64_encode(base64_encode("consultarProducto")) ?>&cantidad=" + $(this).val() + "<?php echo (($orden!="")?"&orden=" . $orden:"") . (($dir!="")?"&dir=" . $dir:"") ?>";
	//alert (url);
	location.replace(url);
});
</script>



 
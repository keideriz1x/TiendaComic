<?php
if (isset($_SESSION)) {
  $nombre = $_SESSION["datos"]->nombre;
  $apellido = $_SESSION["datos"]->apellido;
}


?>
<nav class="menu navbar navbar-expand-lg navbar-dark bg-danger">
  <a class="navbar-brand px" href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoDom.php") ?>&priv=<?php echo base64_encode(base64_encode("sesionDomiciliario")) ?>"><img src="app/img/jd.png" width="150px"></a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoDom.php") ?>&priv=<?php echo base64_encode(base64_encode("verPedidos")) ?>">Elegir pedido</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoDom.php") ?>&priv=<?php echo base64_encode(base64_encode("historialPedidos")) ?>">Historial de pedidos </a>
      </li>
    </ul>
    <ul class="navbar-nav">
      <li class="nav-item active d-none d-md-none d-lg-block my-auto">

        <?php
        if ($_SESSION["datos"]->foto == null) {
        ?>
          <i class="fas fa-user-circle fa-2x pt-1" style="color: white;">
          <?php
        } else {
          ?>
            <img src="data:image/png;base64,<?php echo $_SESSION["datos"]->foto ?>" class="menuPerfil mx-auto d-block " >
          <?php
        }
          ?>
          </i> <!-- Foto aqui, tamaño de foto de 2em -->
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Domiciliario: <?php echo " " . $nombre . " " . $apellido ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoDom.php") ?>&priv=<?php echo base64_encode(base64_encode("editPerfilDom")) ?>">Editar Perfil</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?pid=<?php echo base64_encode("app/presentacion/logout.php") ?>">Cerrar Sesión</a>
      </li>
    </ul>
  </div>

</nav>
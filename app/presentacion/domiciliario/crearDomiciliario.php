<script src="app/presentacion/domiciliario/crearDomiciliario.js"></script>
<div class="container">
	<div class="row mt-4">
		<div class="col-3"></div>
		<div class="col-lg-6">
			<div class="card pt-4">
				<div class="cardAdmin card-header text-center text-white rounded">
					<h3>Crear Domiciliario</h3>
				</div>
				<div class="card-body rounded">
					<form id="create_domiciliario" action=<?php echo "index.php?pid=" . base64_encode("app/presentacion/permisoAdmin.php") ."&priv=". base64_encode(base64_encode("crearDomiciliario")) ?> method="post">
						<div class="form-group">
							<input type="text"  id="nombre" class="form-control" placeholder="Nombre" required="required" autocomplete="off" maxlength="20" minlength="3">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="apellido" placeholder="Apellido" required="required" autocomplete="off" maxlength="20" minlength="3">
						</div>
						<div class="form-group">
							<input type="email" class="form-control" id="correo" placeholder="Correo" required="required" autocomplete="off" maxlength="30" minlength="14" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" id="clave" placeholder="Clave" required="required" autocomplete="off" minlength="4"  maxlength="15">
						</div>
						<div class="form-group">
							<button type="submit" id="crear" class="btn btn-primary btn-block">Crear</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<!-- version 2 -->
<script src="app/presentacion/producto/crearProducto.js"></script>
<div class="container pb-1">
	<div class="row mt-4">
		<div class="col-3"></div>
		<div class="col-lg-6">
			<div class="card pt-4">
				<div class="cardAdmin card-header text-center text-white rounded">
					<h3>Crear Producto</h3>
				</div>
				<div class="card-body">
				
					<form id="create_producto" enctype="multipart/form-data" action=<?php echo "index.php?pid=" . base64_encode("app/presentacion/permisoAdmin.php") ."&priv=". base64_encode(base64_encode("crearProducto")) ?> method="post">
						<div class="form-group">
							<input type="text" maxlength="30" minlength="10" id="nombre" class="form-control" placeholder="Nombre" required="required" autocomplete="off">
						</div>
						<div class="form-group">
							<input type="text" maxlength="10" minlength="4" class="form-control" id="precio" placeholder="Precio" required="required" autocomplete="off" pattern="[0-9]*" title="Formato numerico">
						</div>
						<div class="form-group">
							<input type="text" maxlength="2" class="validanumericos form-control" id="stock" placeholder="Cantidad" required="required" autocomplete="off" pattern="[0-9]*" title="Formato numerico">
						</div>
						<div class="input-group mb-3">
							<div class="input-group-prepend">
								<label class="input-group-text" style="width:5.8em;justify-content: center;">Tipo</label>
							</div>
							<select id="tipo" class="custom-select" required>
								<option hidden selected value="" disabled="disabled">Seleccione el tipo</option>
								<?php
           
								$data = new ControladorTipo();
								$id=$data->listarTipo();
								$tipo= $id->getDatos();
									foreach ($tipo as $clave => $valor) {
									?>  <option value="<?php echo "$valor->idTipo" ?> "> <?php echo "$valor->nombre "; ?></option>  <?php
																					   }
																						   ?>
							</select>
						</div>
						<div class="input-group mb-3">
							<div class="input-group-prepend">
								<label class="input-group-text">Categoria</label>
							</div>

							
							<select id="categoria" class="custom-select" required>
								<option hidden selected value="" disabled="disabled">Seleccione la categoria</option>
								<?php
           
									$data = new ControladorCategoria();
									$id=$data->listarCategoria();
									$categoria= $id->getDatos();
										foreach ($categoria as $clave => $valor) {
										?>  <option value="<?php echo "$valor->idCategoria" ?> "> <?php echo "$valor->nombre "; ?></option>  <?php
																												}
																													?>
							</select>
								
							

						</div>
						<div class="form-group">
							<!-- <textarea id="summernote" name="editordata"></textarea> -->
							<script>
								$('#summernote').summernote({
									placeholder: 'Agregue descripcion (Si se inserta una imagen por medio de una URL, tenga en cuenta que tendra que redimesionar al 100% dicha imagen)',
									tabsize: 2,
									height: 100,
									toolbar: [
										['style', ['style']],
										['font', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
										['fontname', ['fontname']],
										['fontsize', ['fontsize']],
										['para', ['ol', 'ul']],
										['insert', ['link', 'picture', 'hr']],
										['view', ['undo', 'redo']]
									],
									callbacks: {
										onImageUpload: function(files) {
											if (!files.length) return;
											var file = files[0];
											// create FileReader
											var reader = new FileReader();
											reader.onloadend = function() {
												// when loaded file, img's src set datauri
												console.log("img", $("<img>"));
												var img = $("<img>").attr({
													src: reader.result,
													width: "100%"
												}); // << Add here img attributes !
												console.log("var img", img);
												
												$('#summernote').summernote("insertNode", img[0]);
											}

											if (file) {
												// convert fileObject to datauri
												reader.readAsDataURL(file);
											}
										}
									}
								});
								
							</script>
						</div>
						<div class="input-group mb-3">
							<div class="container">
								<div class="row">
									<div class="col-dm-5 px-3"><label style="width: 100%" for="files" class="btn btn-primary ">Seleccionar Imagen</label></div>
									<div class="col-dm-5 px-3"><input style="width: 100%" placeholder="Elegir imagen" autocomplete="off" id="m1" class="read form-control" required />
										<input id="files" style="visibility:hidden;position:absolute;display:none" type="file" value="" onChange="actualiza(this.files[0].name)"></div>
								</div>
							</div>
						</div>
						<div class="form-group text-center">
							<button type="submit" id="crear" class="btn btn-primary btn-block">Crear</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	function actualiza(nombre) { //Pasar nombre de imagen a input
		console.log(nombre);
		document.getElementById('m1').value = nombre;
	}
</script>
<script>
	$("#files").change(function() { //Cambiar de boton
		filename = this.files[0].name
		console.log(filename);
	});
</script>
<script>
	$(".read").on('keydown paste', function(e) { //Required de solo lectura
		e.preventDefault();
	});
</script>

$(document).ready(function () {
  console.log("jQuery is Working");




  // DESCRIPCION


  $(document).on("click", ".descripcion", function (e) {
    var val = window.atob($(this).val());

    data = val.split(":");

    $.ajax({
      type: "post",
      url: "app/servicios/generales/descripcion/ajax_descripcion.php",
      data: { data },

      success: function (response) {
        // console.log(response.datos);


        var template = descripcion(response.datos);

        index = "#";
        index += data[1];
        $(index).html(template);
      },
    });
  });






  //EDICION


  $(document).on("click", ".editar", function (e) {
    var val = window.atob($(this).val());

    data = val.split(":");
// console.log(data[1]);
    mostrar(data[1]);

    



  });






  var guide= "";
  $(document).on("click", ".confirmar_edit", function (e) {
    e.preventDefault();
    var guide= $(".id",this).val();
    var file_data = $('#files').prop('files')[0];
    guia= "";
    guia+="#summer";
    guia+=$(".id",this).val();
   

   
    var form_data = new FormData();
    form_data.append('foto', file_data);
    form_data.append('nombre', $("#nombre").val());
    form_data.append('cantidad', $("#cantidad").val());
    form_data.append('precio', $("#precio").val());
    form_data.append('id', $(".id",this).val());
    form_data.append('descripcion', $(guia).val());
    form_data.append('categoria', $("#categoria").val());
    form_data.append('tipo', $("#tipo").val());
 

//     console.table ([$("#nombre").val() , $("#cantidad").val(),$("#precio").val(),$(".id",this).val(),$(guia).val(), $("#categoria").val(),$("#tipo").val()]);
 
// console.log($(".id",this).val());
 
   


    Swal.fire({
      title: "Estas seguro?",
      text: "¿Desea actualizar sus datos?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, añadirlo!",
      cancelButtonText: "Cancelar!",
    }).then((result) => {




      if (result.isConfirmed) {

        $.ajax({
          url: "app/servicios/individuales/producto/editar_producto/ajax_editar_data.php",
          type: "post",
          dataType: "html",
          data: form_data,
          cache: false,
          contentType: false,
          processData: false,
          success: function (respuesta) {



            if (respuesta != -1) {
              Swal.fire(
                "Operación exitosa!",
                "Sus datos han sido actualizados.",
                "success"
              );
           
              mostrar(guide);


              // setTimeout(() => {
              //   location.reload();
              // }, 1000);

            } else {
              Swal.fire(
                "Operación cancelada!",
                "Se ha producido un error.",
                "error"

              );


            }


          }

        });

      } else {

      }
    });




  });













  // FILTRAR PRODUCTOS


  listar();

  $("#filtroProducto").keyup(function (e) {

    let busqueda = $("#filtroProducto").val();


    if (busqueda.length > 0) {




      // console.table(busqueda); 


      $(".pag").hide();
      $("#cantidad").hide();

      $.ajax({
        type: "post",
        url: "app/servicios/individuales/producto/ajax_consultar_producto.php",
        data: {
          busqueda

        },
        success: function (response) {

          const resultado = JSON.parse(response);
          // console.log(resultado);

          recorrer(resultado);

        }
      });
    } else {
      listar();

    }

  });


  //LISTAR PRODUCTOS

  function listar() {

    $(".pag").show();
    $("#cantidad").show();


    var cantidad = document.getElementById("cantidad").value;
    var pagina = document.getElementById("pagina").value;

    // console.log(cantidad,pagina);


    $.ajax({
      url: "app/servicios/individuales/producto/ajax_mostrar_producto.php",
      type: "post",
      data: {
        cantidad: cantidad,
        pagina: pagina
      },

      success: function (response) {



        const lista = JSON.parse(response);
        // const lista =response;

        // console.log(lista);


        recorrer(lista);

      },

    });

  }

});






//GENERAR DATOS

function recorrer(resultado) {
  let template = "";

  resultado.forEach((item) => {
    var id = "modal";
    id += item["idProducto"];

    var edit = "edit";
    edit += item["idProducto"];

    // idA = btoa(identificador("product", item["idProducto"], item["estado"]) );
    idD = btoa(identificador("product", item["idProducto"]));
    idE = btoa(identificador("editar", item["idProducto"]));




    template += `
    
            <tr>
            <td>${item["idProducto"]} </td>
            <td>${item["nombre"]}</td>
            <td>${item["precio"]}</td>
            <td>${item["stock"]}</td>
            <td>${item["Tipo_idTipo"]}</td>
            <td>${item["Categoria_idCategoria"]}</td>
            <td> 
            <div class="btn-group" role="group" aria-label="Basic example">
            <button class="btn btn-sm descripcion" data-toggle="modal" data-target="#${id}"    value="${idD}"> 
            <i class="fas fa-eye" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Más"></i>
        </button> 



        <button class="btn btn-sm editar" data-toggle="modal" data-target="#${edit}"  value=${idE}> 
        <i class="fas fa-pencil-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Editar"></i>
        </button>
        </div>


      <div class="modal fade" id="${id}" aria-labelledby="exampleModalLabel" aria-modal="true" role="dialog">
                            <div class="modal-dialog modal-xl modal-dialog-centered">
                                <div class="modal-content">
                                <span id=${item["idProducto"]}></span>
                                </div>
                            </div>
                        </div>


      <div class="modal fade" id="${edit}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">


        <span id=ed${item["idProducto"]}></span>


        </div>
        </div> 
        </div>
  
      
            </td>
            
            </tr>
        `;
  });

  $("#listaProducto").html(template);
}



function identificador(rol, id, extra = "") {
  var guia = rol + ":" + id;
  if (extra != "") {
    guia += ":" + extra;
  }
  return guia;
}




//GENERAR DATOS DE LA DESCRIPCION


function descripcion(response) {
  var template = " ";

  template += `
  <div class="modal-header">
  <h1>${response.nombre}</h1>
  <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
</div>
<div class="modal-body">
  <div class="row spacing-15">
      <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
          <img src="data:image/png;base64,${response.foto}" class="mx-auto d-block" style="width:100%" >
      </div>
      <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12" >
<div>Precio: ${response.precio}</div>
          <div>Cantidad: ${response.stock}</div>
          <div>Tipo: ${response.Tipo_idTipo}</div>
          <div>Categoria: ${response.Categoria_idCategoria}</div>
          <div class="text-center">Descripcion</div>
          <div>
          <p>
          
          ${response.descripcion}
          
          </p>
          </div>
      </div>
  </div>
</div>

`;
  return template;
}


//GENERAR DATOS EDITAR PRODUCTO

function editar(response) {

  var producto = response.producto;
  var listaCategoria = response.listaCategoria;
  var listaTipo = response.listaTipo;
  var nomCategoria = response.nombreCategoria;
  var nomTipo = response.nombreTipo;
  var template = "";
  template += `
        <div class="modal-header d-block">
        <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>     



        <h1>Editar Producto</h1>


        </div>

        <div class="contenedor modal-body" style="height:700px;overflow-y:auto">
        <form id="edit_product"  method="post" enctype="multipart/form-data" action="#"> 
        <div class="input-group mb-3 pt-3">
            <div class="container">
            <img src="data:image/png;base64,${producto.foto}" class="mx-auto d-block" style="width:50vh; max-width:170px;height:50vh;max-height:170px;position:relative;box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);" >
                <div class="row pt-3">
                    <div class="col-dm-5 px-3"><label style="width: 100%" for="files" class="btn btn-primary ">Seleccionar Foto</label></div>
                    <div class="col-dm-5 px-3"><input style="width: 100%" placeholder="Elegir foto" autocomplete="off" id="m1" class="read form-control" />
                        <input id="files" style="visibility:hidden;position:absolute" type="file" value="" onChange="actualiza(this.files[0].name)">
                    </div>
                </div>
            </div>
        </div>
       
        <div class="form-group">
            <input type="text" id="nombre" value="${producto.nombre}" class="form-control" placeholder="Nombre" required="required" autocomplete="off"  >
        </div>
        <div class="form-group">
            <input type="text" class="form-control" id="precio" value="${producto.precio}" placeholder="precio" required="required" autocomplete="off">
        </div>
        <div class="form-group">
        <input type="text" class="form-control" id="cantidad" value="${producto.stock}" placeholder="stock" required="required" autocomplete="off">
    </div>
      
   
    <div class="form-group">
    <select id="categoria" class="custom-select" required>
    <option hidden selected value="${producto.Categoria_idCategoria}" >${nomCategoria.nombre}</option>

        `;

  listaCategoria.forEach(element => {
    template += `  <option value="${element.idCategoria}"> ${element.nombre} </option>  `;
  });


  template += `</select>
        
        </div>

        <div class="form-group">
        
        <select id="tipo" class="custom-select" required>
        <option hidden selected value="${producto.Tipo_idTipo}" >${nomTipo.nombre}</option>
        
        `;



  listaTipo.forEach(element => {
    template += `  <option value="${element.idTipo}"> ${element.nombre} </option>  `;
  });


  template += `
  </select>
  </div>

  `;


  template += `
      

      	<div class="form-group">
              <textarea id="summer${producto.idProducto}" class="summer "   name="editordata"></textarea>

              <script>
								$('#summer${producto.idProducto}').summernote({
							
								
									toolbar: [
									
										['font', ['bold', 'italic', 'underline', 'strikethrough']],
										['fontname', ['fontname']],
										['fontsize', ['fontsize']],
										['para', ['ol', 'ul']],
										['insert', ['link', 'picture']],
									
									],
									callbacks: {
										onImageUpload: function(files) {
											if (!files.length) return;
											var file = files[0];
											// create FileReader
											var reader = new FileReader();
											reader.onloadend = function() {
												// when loaded file, img's src set datauri
												console.log("img", $("<img>"));
												var img = $("<img>").attr({
													src: reader.result,
													width: "100%"
												}); // << Add here img attributes !
												console.log("var img", img);
												
												$('#summer${producto.idProducto}').summernote("insertNode", img[0]);
											}

											if (file) {
												// convert fileObject to datauri
												reader.readAsDataURL(file);
											}
										}
									}
								});
								
				
						
                
                $('#summer${producto.idProducto}').summernote('code', '${producto.descripcion}');
							</script>
						</div>


        <div class="form-group text-center">
            <button type="submit"  class="btn btn-primary btn-block confirmar_edit">
            
            <input class="id"  type="hidden" value="${producto.idProducto}"> </input>
            Editar</button>
        </div>
    </form>




    <script>
    function actualiza(nombre) { //Pasar nombre de imagen a input
        console.log(nombre);
        document.getElementById('m1').value = nombre;
    }
</script>
<script>
    $("#files").change(function() { //Cambiar de boton
        filename = this.files[0].name
        console.log(filename);
    });
</script>
<script>
    $(".read").on('keydown paste', function(e) { //Required de solo lectura
        e.preventDefault();
    });
</script>


        </div>




`;

  $("#foto").empty();
  $("#foto").append(template);

  $("#m1").trigger("reset");
  return template;
}



function mostrar(data) {
// console.log(data);
  $.ajax({
    type: "post",
    url: "app/servicios/individuales/producto/editar_producto/ajax_cargar_data.php",
    data: { data },

    success: function (response) {
      var resultado = JSON.parse(response)
 

      var template = editar(resultado);

      index = "#ed";
      index += data;
      $(index).html(template);


    },
  });
}
$(document).ready(function () {
    
    $("#comprar").click(function (e) { 
      
        e.preventDefault();
        Swal.fire({
            title: "Estas seguro?",
            text: "¿Deseas proceder con tu compra ?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Si, proceder!",
            cancelButtonText: "Cancelar!",
          }).then((result) => {
      
            
      
      
            if (result.isConfirmed) {

        $.ajax({
            type: "post",
            url: "app/servicios/generales/carrito/ajax_registrar_compra.php",
           
          
            success: function (response) {

                if(response!=-1) 
                {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Su compra ha sido realizada con exito',
                        showConfirmButton: false,
                      
                      })
                }
                else{
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Error al comprar, intente de nuevo',
                        showConfirmButton: false,
                      
                      })

                }
            }
        });
       
    }
});

    });



});
<?php

$cantidad = 1;
if (isset($_GET["cantidad"])) {
	$cantidad = $_GET["cantidad"];
}
$pagina = 1;
if (isset($_GET["pagina"])) {
	$pagina = $_GET["pagina"];
}
$orden = "";
if (isset($_GET["orden"])) {
	$orden = $_GET["orden"];
}
$dir = "";
if (isset($_GET["dir"])) {
	$dir = $_GET["dir"];
}

$ControladorProducto = new ControladorProducto();


$totalRegistros = $ControladorProducto->ListarReg();
$totalPaginas = intval(($totalRegistros->getDatos() / $cantidad));
if ($totalRegistros->getDatos() % $cantidad != 0) {
	$totalPaginas++;
}

?>
<script src="app/presentacion/producto/mostrarProducto.js"></script>
<div class="container">
	<div class="row mt-4">
		<div class="col-lg-12">
			<div class="card pt-4 rounded">
				<div class="cardAdmin card-header text-center text-white rounded">
					<h3>Consultar Producto</h3>
				</div>
				<div class="card-body">
					<input type="search" id="filtroProducto" class="form-control" placeholder="Filtro">
					<div class="contenedor table-responsive pt-2">
						<table class="table table-striped table-hover text-nowrap">
							<thead class="thead-dark">
								<tr>
									<th width="8%">#</th>
									<th width="15%">Nombre</th>
									<th width="15%">Precio</th>
									<th width="15%">Cantidad</th>
									<th width="15%">Tipo</th>
									<th width="15%">Categoria</th>
									<th>Servicios</th>
								</tr>
							</thead>
							<tbody id="listaProducto">

							</tbody>
						</table>

					</div>
					<div class="row pt-3">
						<div class="col-md-10 mr-auto">
							<nav aria-label="Page navigation example">
								<?php
								// calculamos la primera y última página a mostrar
								$primera = $pagina - ($pagina % 5) + 1;
								if ($primera > $pagina) {
									$primera = $primera - 5;
								}
								$ultima = $primera + 4 > $totalPaginas ? $totalPaginas : $primera + 4;
								?>
								<ul class="pagination pagination-sm">
									<?php
									if ($totalPaginas > 1) {
										// comprobamos $primera en lugar de $pagina
										if ($pagina == 1) {
											echo "<li class='pag page-item disabled'><a class='page-link' href='#' aria-label='Previous'><span aria-hidden='true'>&laquo;</span></a></li>";
										} elseif ($pagina > 1) {
											echo "<li class='pag page-item'><a class='page-link' href='index.php?pid=" . base64_encode("app/presentacion/permisoAdmin.php") . "&priv=" . base64_encode(base64_encode("consultarProducto"))  . "&pagina=" . ($pagina - 1) . "&cantidad=" . $cantidad . (($orden != "") ? "&orden=" . $orden : "") . (($dir != "") ? "&dir=" . $dir : "") . "' aria-label='Previous'><span aria-hidden='true'>&laquo;</span></a></li>";
										}
										// mostramos de la primera a la última
										for ($i = $primera; $i <= $ultima; $i++) {
											if ($pagina == $i)
												echo "<li class='pag page-item active'><a class='page-link' href='index.php?pid=" . base64_encode("app/presentacion/permisoAdmin.php") . "&priv=" . base64_encode(base64_encode("consultarProducto")) . "&pagina=" . $i . "&cantidad=" . $cantidad . (($orden != "") ? "&orden=" . $orden : "") . (($dir != "") ? "&dir=" . $dir : "") . "'>" . $i . "</a></li>";
											else
												echo "<li class='pag page-item'><a class='page-link' href='index.php?pid=" . base64_encode("app/presentacion/permisoAdmin.php") . "&priv=" . base64_encode(base64_encode("consultarProducto"))  . "&pagina=" . $i . "&cantidad=" . $cantidad . (($orden != "") ? "&orden=" . $orden : "") . (($dir != "") ? "&dir=" . $dir : "") . "'>" . $i . "</a></li>";
										}

										if ($i > $pagina && $pagina == $totalPaginas) {
											echo "<li class='pag page-item disabled'><a class='page-link' href='#'><span aria-hidden='true'>&raquo;</span></a></li>";
										} else {
											echo "<li class='pag page-item'><a class='page-link' href='index.php?pid=" . base64_encode("app/presentacion/permisoAdmin.php") . "&priv=" . base64_encode(base64_encode("consultarProducto"))  . "&pagina=" . ($pagina + 1) . "&cantidad=" . $cantidad . (($orden != "") ? "&orden=" . $orden : "") . (($dir != "") ? "&dir=" . $dir : "") . "'><span aria-hidden='true'>&raquo;</span></a></li>";
										}
									}
									?>
								</ul>
							</nav>
						</div>
						<div class="col-md-2 text-right mr-auto">
							<select name="cantidad" id="cantidad" class="form-control form-control-sm">
								<option value="5" <?php echo ($cantidad == 5) ? "selected" : "" ?>>5</option>
								<option value="10" <?php echo ($cantidad == 10) ? "selected" : "" ?>>10</option>
								<option value="20" <?php echo ($cantidad == 20) ? "selected" : "" ?>>20</option>
							</select>
							<input id="pagina" type="hidden" value="<?php echo $pagina  ?>"></input>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$("#cantidad").on("change", function() {
		url = "index.php?pid=<?php echo base64_encode("app/presentacion/permisoAdmin.php") ?>&priv=<?php echo base64_encode(base64_encode("consultarProducto")) ?>&cantidad=" + $(this).val() + "<?php echo (($orden != "") ? "&orden=" . $orden : "") . (($dir != "") ? "&dir=" . $dir : "") ?>";
		//alert (url);
		location.replace(url);
	});
</script>
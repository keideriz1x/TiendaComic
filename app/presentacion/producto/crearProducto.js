$(document).ready(function () {
  console.log("jQuery is Working");

  // listar();


 


  $("#create_producto").submit(function (e) {
    e.preventDefault();


    var file_data = $('#files').prop('files')[0];  
    
    // console.log(file_data);
    

    var form_data = new FormData();
    form_data.append('foto', file_data);
    form_data.append('nombre',$("#nombre").val());
    form_data.append('precio',$("#precio").val());
    form_data.append('stock',$("#stock").val());
    form_data.append('categoria',$("#categoria").val());
    form_data.append('tipo',$("#tipo").val());
    // form_data.append('descripcion',$("#summernote").val());
    

  
    //console.log(postData);
    
    Swal.fire({
      title: "Estas seguro?",
      text: "¿Quieres añadir un nuevo producto?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, añadirlo!",
      cancelButtonText: "Cancelar!",
    }).then((result) => {

      


      if (result.isConfirmed) {

        $.ajax({
          url: "app/servicios/individuales/producto/ajax_crear_producto.php",
          type: "post",
          dataType: "html",
          data: form_data,
          cache: false,
          contentType: false,
          processData: false,
          success: function(res){
            let respuesta= res;
            // console.log(respuesta);
            if(respuesta!=-1)
            {
              Swal.fire(
                "Operación exitosa!",
                "El producto ha sido añadido.",
                "success"
              );
    
    
            }else{
              Swal.fire(
                "Operación cancelada!",
                "Se ha producido un error.",
                "error"
              );
            }
            $("#create_producto").trigger("reset");

        }
        
        });

      }else{
        $("#create_producto").trigger("reset");
      }
    });
    
  });
});

<?php
if (isset($_SESSION["id"])) {
    if ($_SESSION["rol"] == "domiciliario") {
        switch (base64_decode(base64_decode($_GET["priv"]))) {
            case "sesionDomiciliario":
                include "app/presentacion/sesionDomiciliario.php";
                break;
            case "verPedidos":
                include "app/presentacion/verPedidos.php";
                break;
            case "historialPedidos":
                include "app/presentacion/historialPedidos.php";
                break;
            case "editPerfilDom":
                include "app/presentacion/editPerfilDom.php";
                break;
            default:
            include "app/presentacion/noExiste.php";
            break;
        }
    
    ?>
    <?php
    }elseif ($_SESSION["rol"] == "cliente" || $_SESSION["rol"] == "administrador") {
        if ($_SESSION["rol"] == "cliente") {
            include "app/presentacion/menuinicio.php";
            include "app/presentacion/permisos.php";
        }
        elseif ($_SESSION["rol"] == "administrador") {
            include "app/presentacion/permisos.php";
        }
    }
?>
<?php
}else{
    header("Location: index.php");
}
?>
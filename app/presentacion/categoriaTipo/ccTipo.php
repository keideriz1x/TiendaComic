<script src="app/presentacion/categoriaTipo/tipo.js"></script>

<?php

$cantidad = 5;
if (isset($_GET["cantidad"])) {
	$cantidad = $_GET["cantidad"];
}
$pagina = 1;
if (isset($_GET["pagina"])) {
	$pagina = $_GET["pagina"];
}
$orden = "";
if (isset($_GET["orden"])) {
	$orden = $_GET["orden"];
}
$dir = "";
if (isset($_GET["dir"])) {
	$dir = $_GET["dir"];
}

$Controlador = new ControladorTipo();


$totalRegistros = $Controlador->ListarReg();
$totalPaginas = intval(($totalRegistros->getDatos() / $cantidad));
if ($totalRegistros->getDatos() % $cantidad != 0) {
	$totalPaginas++;
}

?>

<div class="container">
	<div class="row mt-4">
		<div class="col-3"></div>
		<div class="col-lg-6">
			<div class="card pt-4">
				<div class="cardAdmin card-header text-center text-white rounded">
					<h3>Crear Tipo</h3>
				</div>
				<div class="card-body">
					<form id="create_tipo" action=<?php echo "index.php?pid=" . base64_encode("app/presentacion/permisoAdmin.php") ."&priv=". base64_encode(base64_encode("ccTipo")) ?> method="post">
						<div class="form-group">
							<input type="text" id="nombre" maxlength="15" minlength="3" name="nombre" class="form-control" placeholder="Nombre" required="required" autocomplete="off">
							<input type="hidden" id="id" value="<?php echo $_SESSION["id"] ?>">
						</div>
						<div class="form-group">
							<button type="submit" name="crear" class="btn btn-primary btn-block">Crear</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div class="container pb-1">
	<div class="row mt-4">
		<div class="col-3"></div>
		<div class="col-lg-6">
			<div class="card pt-4 rounded">
				<div class="cardAdmin card-header text-center text-white rounded">
					<h3>Consultar Tipo</h3>
				</div>
				<div class="card-body">
					<input type="search" id="filtroTipo" class="form-control" placeholder="Filtro">
					<div class="contenedor table-responsive pt-2">
						<table class="table table-striped table-hover text-nowrap">
							<thead class="thead-dark">
								<tr>
									<th width="20%">#</th>
									<th width="20%">Nombre</th>
									<th width="40%">Estado</th>

									<th>Servicios</th>
								</tr>
							</thead>
							<tbody id="listaTipo">

							</tbody>
						</table>
					</div>
					<div class="row pt-1">
						<div class="col-sm-2 mr-auto pl-3">
							<nav aria-label="Page navigation example">
								<?php
								// calculamos la primera y última página a mostrar
								$primera = $pagina - ($pagina % 5) + 1;
								if ($primera > $pagina) {
									$primera = $primera - 5;
								}
								$ultima = $primera + 4 > $totalPaginas ? $totalPaginas : $primera + 4;
								?>					
									<ul class="pagination pagination-sm">
										<?php
										if ($totalPaginas > 1) {
											// comprobamos $primera en lugar de $pagina
											if ($pagina == 1) {
												echo "<li class='page-item disabled'><a class='pag page-link' href='#' aria-label='Previous'><span aria-hidden='true'>&laquo;</span></a></li>";
											} elseif ($pagina > 1) {
												echo "<li class='page-item'><a class='pag page-link' href='index.php?pid=" . base64_encode("app/presentacion/permisoAdmin.php") ."&priv=". base64_encode(base64_encode("ccTipo")) . "&pagina=" . ($pagina - 1) . "&cantidad=" . $cantidad . (($orden != "") ? "&orden=" . $orden : "") . (($dir != "") ? "&dir=" . $dir : "") . "' aria-label='Previous'><span aria-hidden='true'>&laquo;</span></a></li>";
											}
											// mostramos de la primera a la última
											for ($i = $primera; $i <= $ultima; $i++) {
												if ($pagina == $i)
													echo "<li class='pag page-item active'><a class='page-link' href='index.php?pid=" . base64_encode("app/presentacion/permisoAdmin.php") ."&priv=". base64_encode(base64_encode("ccTipo")) . "&pagina=" . $i . "&cantidad=" . $cantidad . (($orden != "") ? "&orden=" . $orden : "") . (($dir != "") ? "&dir=" . $dir : "") . "'>" . $i . "</a></li>";
												else
													echo "<li class='pag page-item'><a class='page-link' href='index.php?pid=" . base64_encode("app/presentacion/permisoAdmin.php") ."&priv=". base64_encode(base64_encode("ccTipo")) . "&pagina=" . $i . "&cantidad=" . $cantidad . (($orden != "") ? "&orden=" . $orden : "") . (($dir != "") ? "&dir=" . $dir : "") . "'>" . $i . "</a></li>";
											}

											if ($i > $pagina && $pagina == $totalPaginas) {
												echo "<li class='pag page-item disabled'><a class='page-link' href='#'><span aria-hidden='true'>&raquo;</span></a></li>";
											} else {
												echo "<li class='pag page-item'><a class='page-link' href='index.php?pid=" . base64_encode("app/presentacion/permisoAdmin.php") ."&priv=". base64_encode(base64_encode("ccTipo")) . "&pagina=" . ($pagina + 1) . "&cantidad=" . $cantidad . (($orden != "") ? "&orden=" . $orden : "") . (($dir != "") ? "&dir=" . $dir : "") . "'><span aria-hidden='true'>&raquo;</span></a></li>";
											}
										}
										?>
									</ul>
								</nav>
						</div>
						<div class="col-sm-2 text-right ml-auto pr-3">
							<select name="cantidad" id="cantidad" class="form-control form-control-sm">
								<option value="5" <?php echo ($cantidad == 5) ? "selected" : "" ?>>5</option>
								<option value="10" <?php echo ($cantidad == 10) ? "selected" : "" ?>>10</option>
								<option value="20" <?php echo ($cantidad == 20) ? "selected" : "" ?>>20</option>
							</select>
							<input id="pagina" type="hidden" value="<?php echo $pagina  ?>"></input>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$("#cantidad").on("change", function() {
		url = "index.php?pid=<?php echo base64_encode("app/presentacion/permisoAdmin.php") ."&priv=". base64_encode(base64_encode("ccTipo")) ?>&cantidad=" + $(this).val() + "<?php echo (($orden != "") ? "&orden=" . $orden : "") . (($dir != "") ? "&dir=" . $dir : "") ?>";
		//alert (url);
		location.replace(url);
	});
</script>
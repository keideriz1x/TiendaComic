$(document).ready(function () {
  console.log("jQuery is Working");



  $(document).on("click", ".boton", function () {
    var val = window.atob($(this).val());

    data = val.split(":");
    // console.log(data);
    $.ajax({
      type: "post",
      url: "app/servicios/generales/estado/ajax_cambiar_estado.php",
      data: { data },

      success: function (response) {
        // console.log(response);
        listar();
      },
    });
  });





  listar();
  $("#filtroCategoria").keyup(function (e) {
    let busqueda = $("#filtroCategoria").val();
    // console.log(busqueda);

    if (busqueda.length > 0) {
      $(".pag").hide();
      $("#cantidad").hide();
      $.ajax({
        type: "post",
        url: "app/servicios/individuales/categoria/ajax_consultar_categoria.php",
        data: { busqueda },
        success: function (response) {
          const resultado = response.datos;

          template(resultado);
        },
      });
    } else {
      listar();
    }
  });

  $("#create_categoria").submit(function (e) {
    e.preventDefault();

    const postData = {
      name: $("#nombre").val(),
      
    };

    Swal.fire({
      title: "Estas seguro?",
      text: "¿Quieres añadir una nueva categoria?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, añadirla!",
      cancelButtonText: "Cancelar!",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          "Operación exitosa!",
          "La nueva categoria ha sido añadida.",
          "success"
        );
        $.post(
          "app/servicios/individuales/categoria/ajax_crear_categoria.php",
          postData,
          function (response) {
            listar();
            $("#create_categoria").trigger("reset");
          }
        );
      } else {
        $("#create_categoria").trigger("reset");
      }
    });
  });

  function listar() {

    $(".pag").show();
    $("#cantidad").show();
     
var cantidad= document.getElementById("cantidad").value;
var pagina=  document.getElementById("pagina").value;  

// console.log(cantidad,pagina);

    $.ajax({
      url: "app/servicios/individuales/categoria/ajax_mostrar_categoria.php",
      type: "post",
      data: { cantidad:cantidad,
                 pagina:pagina   } ,

      success: function (response) {
        const lista = response.datos;
        template(lista);

       
      },
    });
  }





  function template (lista){
    let template = "";
    lista.forEach((item) => {

      item["estado"] = status(item["estado"]);

      idA = btoa(
        identificador("categoria", item["idCategoria"], item["estado"])
      );

      template += `
            <tr>
            <td>${item["idCategoria"]} </td>
            <td>${item["nombre"]}</td>
            <td>${item["estado"]}</td>
            <td>

            <button   class ="btn btn-sm boton" style="border: none"  value="${idA}"> 
            <i class="fas fa-ban" data-bs-toggle="tooltip" title="Deshabilitar"></i>
              </button>
            
            </td>
            </tr>
        `;
    });
    $("#listaCategoria").html(template);


  }
});

function status(estado) {
  if (estado != "1") {
    return "Restringido";
  } else {
    return "Activo";
  }
}

function identificador(rol, id, extra = "") {
  var guia = rol + ":" + id;
  if (extra != "") {
    guia += ":" + extra;
  }
  return guia;
}



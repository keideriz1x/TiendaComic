$(document).ready(function () {
    console.log("jQuery is Working");
  


    $(document).on("click", ".boton", function () {
      var val = window.atob($(this).val());
  
      data = val.split(":");
      // console.log(data);
      $.ajax({
        type: "post",
        url: "app/servicios/generales/estado/ajax_cambiar_estado.php",
        data: { data },
  
        success: function (response) {
          // console.log(response);
          listar();
        },
      });
    });




 
    listar();
    $("#filtroTipo").keyup(function (e) { 
      let busqueda=$("#filtroTipo").val();
      // console.log(busqueda);
      
      if(busqueda.length >0)
      {
        $(".pag").hide();
        $("#cantidad").hide();
        $.ajax({
          type: "post",
          url: "app/servicios/individuales/tipo/ajax_consultar_tipo.php",
          data: {busqueda},
          success: function (response) {
            const resultado = response.datos;
            
              template(resultado);
           
         
              
          }
        });
      }else{
        listar();
      }
  
    });



  
  
    $("#create_tipo").submit(function (e) {
      e.preventDefault();
      
      const postData = {
        name: $("#nombre").val(),
        
      };
      


      Swal.fire({
        title: 'Estas seguro?',
        text: "¿Quieres añadir un nuevo Tipo?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, añadirlo!',
        cancelButtonText: 'Cancelar!'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Operación exitosa!',
            'el nuevo Tipo ha sido añadida.',
            'success'
          )
          





      $.post(
        "app/servicios/individuales/tipo/ajax_crear_tipo.php",
        postData,
        function (response) {
          console.log(response);
          listar();
        
        }
      );
      $("#create_tipo").trigger("reset");

        }else{
          $("#create_tipo").trigger("reset");
        }
      })
    });
  
    function listar() {
      $(".pag").show();
      $("#cantidad").show();
       
  var cantidad= document.getElementById("cantidad").value;
  var pagina=  document.getElementById("pagina").value;  
  
  console.log(cantidad,pagina);
      $.ajax({
        url: "app/servicios/individuales/tipo/ajax_mostrar_tipo.php",
        type: "post",
        data: { cantidad:cantidad,
                   pagina:pagina   } ,
  
        success: function (response) {
          const lista = response.datos;
            template(lista);


        
        
        },
      });
    }


    function template (lista){
      let template = "";
      lista.forEach((item) => {
       
        item["estado"] = status(item["estado"]);

        idA = btoa(
          identificador("tipo", item["idTipo"], item["estado"])
        );

        template += `
              <tr>
              <td>${item["idTipo"]} </td>
              <td>${item["nombre"]}</td>
              <td>${item["estado"]}</td>
              <td>
              

              <button   class ="btn btn-sm boton" style="border: none"  value="${idA}"> 
              <i class="fas fa-ban" data-bs-toggle="tooltip" title="Deshabilitar"></i>
                </button>
              
              </td>
                
              </tr>
          `;
      });
      $("#listaTipo").html(template);

    }


  });
  

  function status(estado) {
    if (estado != "1") {
      return "Restringido";
    } else {
      return "Activo";
    }
  }
  
  function identificador(rol, id, extra = "") {
    var guia = rol + ":" + id;
    if (extra != "") {
      guia += ":" + extra;
    }
    return guia;
  }
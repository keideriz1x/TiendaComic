<script src="app/presentacion/pedido/pedido.js"></script>


<?php

$cantidad = 5;
if (isset($_GET["cantidad"])) {
	$cantidad = $_GET["cantidad"];
}
$pagina = 1;
if (isset($_GET["pagina"])) {
	$pagina = $_GET["pagina"];
}
$orden = "";
if (isset($_GET["orden"])) {
	$orden = $_GET["orden"];
}
$dir = "";
if (isset($_GET["dir"])) {
	$dir = $_GET["dir"];
}

$Controlador = new ControladorCarrito();


$totalRegistros = $Controlador->listarReg(2);
$totalPaginas = intval(($totalRegistros->getDatos() / $cantidad));
if ($totalRegistros->getDatos() % $cantidad != 0) {
	$totalPaginas++;
}

?>

<input id="p" type="hidden" value="1"></input>

<div class="container">
	<div class="row mt-4">
		<div class="col-lg-12">
			<div class="card pt-4 rounded">
				<div class="cardDom card-header text-center text-white rounded">
					<h3>Pedidos</h3>
				</div>
				<div class="card-body">
					<input type="search" id="filtroCliente" class="form-control" placeholder="Filtro">
					<div class="contenedor table-responsive pt-2">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th width="8%">#</th>
									<th width="25%">Fecha</th>
									<th width="25%">Dirección de entrega</th>
									<th width="25%">Estado</th>
									<th>Servicios</th>
								</tr>
							</thead>
							<tbody id="listaPedido">

							</tbody>
						</table>
					</div>
					<div class="row pt-1">
						<div class="col-sm-10 mr-auto pl-3">
							<nav aria-label="Page navigation example">
								<?php
								// calculamos la primera y última página a mostrar
								$primera = $pagina - ($pagina % 5) + 1;
								if ($primera > $pagina) {
									$primera = $primera - 5;
								}
								$ultima = $primera + 4 > $totalPaginas ? $totalPaginas : $primera + 4;
								?>
								<nav aria-label="Page navigation" class="text-center">
									<ul class="pagination pagination-sm">
										<?php
										if ($totalPaginas > 1) {
											// comprobamos $primera en lugar de $pagina
											if ($pagina == 1) {
												echo "<li class='page-item disabled'><a class='pag page-link' href='#' aria-label='Previous'><span aria-hidden='true'>&laquo;</span></a></li>";
											} elseif ($pagina > 1) {
												echo "<li class='page-item'><a class='pag page-link' href='index.php?pid=" . base64_encode("app/presentacion/permisoDom.php") . "&priv=" . base64_encode(base64_encode("verPedidos")) . "&pagina=" . ($pagina - 1) . "&cantidad=" . $cantidad . (($orden != "") ? "&orden=" . $orden : "") . (($dir != "") ? "&dir=" . $dir : "") . "' aria-label='Previous'><span aria-hidden='true'>&laquo;</span></a></li>";
											}
											// mostramos de la primera a la última
											for ($i = $primera; $i <= $ultima; $i++) {
												if ($pagina == $i)
													echo "<li class='pag page-item active'><a class='page-link' href='index.php?pid=" . base64_encode("app/presentacion/permisoDom.php") . "&priv=" . base64_encode(base64_encode("verPedidos")) . "&pagina=" . $i . "&cantidad=" . $cantidad . (($orden != "") ? "&orden=" . $orden : "") . (($dir != "") ? "&dir=" . $dir : "") . "'>" . $i . "</a></li>";
												else
													echo "<li class='pag page-item'><a class='page-link' href='index.php?pid=" . base64_encode("app/presentacion/permisoDom.php") . "&priv=" . base64_encode(base64_encode("verPedidos")) . "&pagina=" . $i . "&cantidad=" . $cantidad . (($orden != "") ? "&orden=" . $orden : "") . (($dir != "") ? "&dir=" . $dir : "") . "'>" . $i . "</a></li>";
											}

											if ($i > $pagina && $pagina == $totalPaginas) {
												echo "<li class='pag page-item disabled'><a class='page-link' href='#'><span aria-hidden='true'>&raquo;</span></a></li>";
											} else {
												echo "<li class='pag page-item'><a class='page-link' href='index.php?pid=" . base64_encode("app/presentacion/permisoDom.php") . "&priv=" . base64_encode(base64_encode("verPedidos")) . "&pagina=" . ($pagina + 1) . "&cantidad=" . $cantidad . (($orden != "") ? "&orden=" . $orden : "") . (($dir != "") ? "&dir=" . $dir : "") . "'><span aria-hidden='true'>&raquo;</span></a></li>";
											}
										}
										?>
								</nav>
						</div>
						<div class="col-sm-2 text-right ml-auto">
							<select name="cantidad" id="cantidad" class="form-control form-control-sm">
								<option value="5" <?php echo ($cantidad == 5) ? "selected" : "" ?>>5</option>
								<option value="10" <?php echo ($cantidad == 10) ? "selected" : "" ?>>10</option>
								<option value="20" <?php echo ($cantidad == 20) ? "selected" : "" ?>>20</option>
							</select>
							<input id="pagina" type="hidden" value="<?php echo $pagina  ?>"></input>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$("#cantidad").on("change", function() {
		url = "index.php?pid=<?php echo base64_encode("app/presentacion/permisoDom.php") ?>&priv=<?php echo base64_encode(base64_encode("verPedidos")) ?>&cantidad=" + $(this).val() + "<?php echo (($orden != "") ? "&orden=" . $orden : "") . (($dir != "") ? "&dir=" . $dir : "") ?>";
		//alert (url);
		location.replace(url);
	});
</script>
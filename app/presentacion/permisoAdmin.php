<?php
if (isset($_SESSION["id"])) {
    if ($_SESSION["rol"] == "administrador") {
        
        switch (base64_decode(base64_decode($_GET["priv"]))) {
            case "sesionAdministrador":
                include "app/presentacion/sesionAdministrador.php";
                break;
            case "crearProducto":
                include "app/presentacion/producto/crearProducto.php";
                break;
            case "consultarProducto":
                include "app/presentacion/producto/consultarProducto.php";
                break;
            case "ccCategoria":
                include "app/presentacion/categoriaTipo/ccCategoria.php";
                break;
            case "ccTipo":
                include "app/presentacion/categoriaTipo/ccTipo.php";
                break;
            case "consultarCliente":
                include "app/presentacion/cliente/consultarCliente.php";
                break;
            case "crearDomiciliario":
                include "app/presentacion/domiciliario/crearDomiciliario.php";
                break;
            case "consultarDomiciliario":
                include "app/presentacion/domiciliario/consultarDomiciliario.php";
                break;
            case "log":
                include "app/presentacion/log.php";
                break;
            case "editPerfilAdmin":
                include "app/presentacion/editPerfilAdmin.php";
                break;
            case "consultarPedido":
                include "app/presentacion/consultarPedido.php";
                break;
            case "validarPedido":
                include "app/presentacion/validarPedido.php";
                break;
            default:
            include "app/presentacion/noExiste.php";
            break;
        }
    
    ?>
    <?php
    }elseif ($_SESSION["rol"] == "cliente" || $_SESSION["rol"] == "domiciliario") {
        if ($_SESSION["rol"] == "cliente") {
            include "app/presentacion/menuinicio.php";
            include "app/presentacion/permisos.php";
        }
        elseif ($_SESSION["rol"] == "domiciliario") {
            include "app/presentacion/permisos.php";
        }
    }
?>
<?php
}else{
    header("Location: index.php");
}
?>
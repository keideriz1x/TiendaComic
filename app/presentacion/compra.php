<?php
if(!isset($_SESSION))
session_start();

?>

<script src="app/servicios/generales/editar_perfil/editar_perfil.js"></script>
<script src="app/presentacion/producto/compra.js"></script>
<div class="col-12 container pt-3">
    <div class="row">
        <div class="col-lg-5 mx-auto d-block pt-2">
            <div class="card">
                <div class="card-body" id="elemento">
                    <h3>Información de contacto</h3>
                    <div class="container">
                        <div class="row">
                            <div>
                                <?php
                                if ($_SESSION["datos"]->foto == null) {
                                ?>
                                    <i class="fas fa-user-circle fa-2x pt-1" style="color: white;">
                                    <?php
                                } else {
                                    ?>
                                        <img class="menuPerfil d-none d-xs-none d-sm-block" src="data:image/png;base64,<?php echo $_SESSION["datos"]->foto ?>">
                                    <?php
                                }
                                    ?>
                            </div>
                            <div class="pl-3">
                                <span><?php echo $_SESSION["datos"]->nombre ?> <?php echo $_SESSION["datos"]->apellido ?></span>
                                <br>
                                <span>(<?php echo $_SESSION["datos"]->correo ?>)</span>
                            </div>
                        </div>
                    </div>
                    <h3 class="py-3">Datos del envio</h3>
                    <form id="edit" enctype="multipart/form-data" action=<?php echo "index.php?pid=" . base64_encode("app/presentacion/permisoCliente.php") . "&priv=" . base64_encode(base64_encode("perfilCliente")) ?> method="post">
                        <input type="hidden" id="id" value="<?php echo $_SESSION["id"] ?>">
                        <input type="hidden" id="rol" value="<?php echo $_SESSION["rol"] ?>">
                        <div class="form-group">
                            <input type="text" id="nombre" value="<?php echo  $_SESSION["datos"]->nombre?>" class="form-control" placeholder="Nombre"  required="required" autocomplete="off" maxlength="20" minlength="3">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="apellido" value="<?php echo  $_SESSION["datos"]->apellido?>" placeholder="Apellido" required="required" autocomplete="off" maxlength="20" minlength="3">
                        </div>
                        <div class="form-group">
                            <input type="text" class="validanumericos form-control" value="<?php echo  $_SESSION["datos"]->direccion?>" id="direccion" placeholder="Direccion" autocomplete="off" maxlength="40" minlength="15">
                        </div>
                        <div class="form-group">
                            <input type="text" class="validanumericos form-control" value="<?php echo  $_SESSION["datos"]->telefono?>" id="telefono" placeholder="Telefono" autocomplete="off" maxlength="10"  minlength="8">
                        </div>
                        <!-- <div class="form-group">
                            <input type="text" class="validanumericos form-control" value="" id="clave" placeholder="Clave" required="required" autocomplete="off">
                        </div> -->
                        <div class="form-group text-center">
                            <button type="submit" id="crear" class="btn btn-primary btn-block">Editar</button>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
        <div class="col-lg-5 mx-auto d-block pt-2">
            <div class="card">
                <div class="card-body">
                    <div class="compra contenedor table-responsive" id="elemento2">
                        <table class="table text-nowrap table-borderless">
                            <tbody>

                           <?php
                           $subtotal=0;
                           if(!isset($_SESSION["carrito"]) || empty($_SESSION["carrito"]))
                           {
                                echo " <h2> carrito vacio </h2>";
                           }
                           else
                           {
                            foreach($_SESSION["carrito"] as $indice=> $producto){
                           ?>
                                <tr>

                            
                                    <td width="21%">
                                        <div class="caja" style="position:relative;display:inline-block">
                                            <img src="data:image/png;base64,<?php echo $producto["foto"]?>" class="imgcompra img-thumbnail">
                                            <span class="cantidadp num text-white" style=""><?php echo $producto["cantidad"]?></span>
                                        </div>
                                    </td>
                                    <th width="58%">
                                        <span> <?php echo $producto["nombre"]?></span>
                                    </th>
                                    <td width="21%">
                                        <span><?php echo $producto["precioTotal"]?> COP </span>
                                    </td>
                                    
                                </tr>
                             
                              <?php
                              $subtotal+= intval ($producto["precioTotal"]);
                            }
                        }
                              ?>
                            
                            </tbody>
                        </table>
                    </div>
                    <div class="contenedor table-responsive">
                        <table class="table text-nowrap">
                            <tbody>
                                <tr>
                                    <th>Subtotal</th>
                                    <td style="text-align:right;">
                                        <span>
                                           <?php echo $subtotal." COP" ?>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <th style="border: none;">
                                        <span>
                                            Envíos
                                        </span>
                                    </th>
                                    <td style="text-align:right;border: none;">
                                        <span>
                                            <?php
                                            $total=0;
                                            if($subtotal<100000)
                                            {
                                                echo "5000 COP";
                                               $total=($subtotal+5000);
                                            }else{
                                                echo "Envio gratis";
                                                $total=$subtotal;
                                            }


                                            ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>
                                        <span>Total</span>
                                    </th>
                                    <td style="text-align:right;">
                                        <span>COP</span>
                                        <span>
                                            <?php
                                            echo $total;
                                            ?>
                                        </span>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class='w-100 pt-3'>
                        <button type='button' class='btn btn-primary col-sm-3' onclick='history.back()'>Regresar</button>
                        <a type='button' id="comprar" href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoCliente.php") ?>&priv=<?php echo base64_encode(base64_encode("compra")) ?>" class='btn btn-danger float-right col-sm-3'>Comprar</a>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    $('#elemento2').css({ 'height':$("#elemento").height()/2 });
});
</script>
$(document).ready(function () {
    console.log("Jquery ready");


  mostrar();


  $(document).on("click", "#fin_pedido", function (e) {

    Swal.fire({
      title: "Estas seguro?",
      text: "¿La entrega fue hecha con exito?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, validar entrega!",
      cancelButtonText: "Cancelar!",
    })
    
    
    .then((result) => {

      var data = window.atob($(this).val());
      console.log(data);

      var estado =2;
      if (result.isConfirmed) {
    


        console.log( "hola peera");
    $.ajax({
      type: "post",
      url: "app/servicios/generales/pedido/ajax_cambiar_estado.php",
      data: { data,estado },

      success: function (response) {

        console.log(response);
        
       if(response>0)
       {
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'El pedido ha sido añadido',
          showConfirmButton: false,
          timer: 1500
        })


       }else
       {
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: 'Complete el pedido actual para poder tomar otro',
          showConfirmButton: false,
          
        })
       }
       mostrar();


      },
    });
  }
});



  });



  $(document).on("click", ".boton", function (e) {
    e.preventDefault();
    var data = window.atob($(this).val());

    
    // console.log(data);

    Swal.fire({
      title: "Estas seguro?",
      text: "¿Quieres tomar este pedido?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, añadirlo!",
      cancelButtonText: "Cancelar!",
    })
    
    
    .then((result) => {

      
      if (result.isConfirmed) {
    
    $.ajax({
      type: "post",
      url: "app/servicios/generales/pedido/ajax_otorgar_pedido.php",
      data: { data },

      success: function (response) {

        
       if(response>0)
       {
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'El pedido ha sido añadido',
          showConfirmButton: false,
          timer: 1500
        })


       }else
       {
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: 'Complete el pedido actual para poder tomar otro',
          showConfirmButton: false,
          
        })
       }
       


      },
    });
  }
});
  });



});






// -----------------------------------------------------------------------------------------------------------------------------------------------------------------


function mostrar() {
  $(".pag").show();
  $("#cantidad").show();

  rol = tabla(parseInt($("#p").val()));
  if(rol!=2)
  {
    var cantidad = document.getElementById("cantidad").value;
    var pagina = document.getElementById("pagina").value;

  }else{
    var cantidad=5;
    var pagina=1;
  }
 
  

    // console.log(cantidad,pagina);
    var rol;


// console.log(rol);

    
    $.ajax({
        type: "post",
        url: "app/servicios/generales/pedido/ajax_mostrar_pedido.php",
        data: {
            cantidad ,pagina, rol
        },
       
        success: function (response) {
            // console.log(response.datos);
            var data= JSON.parse(response);

            console.log(data);



            switch (rol) {

              case 0:
                templatePersonal(data);
                break;
                case 1:
                  template(data);
                  break;

                  case 2:
                    templatePedido(data)
                    break;

                 
            
             
            }



         
        



        }
    });
    
}


function  template (resultado) {
    let template = "";
    fecha=" ";



    resultado.forEach(resultado => {
      var meta=  resultado["Cliente_idCliente"].split(":");
        resultado['estado']=status(resultado['estado']);
        A= resultado.idCarrito;
        id=window.btoa(A);
        fecha=resultado.fechaCompra.split(" ");
        template += `
        <tr>
               <td>4825-2-${resultado.idCarrito}</td>
               <td>${fecha[0]}</td> 
               <td>${meta[1]} </td>
               <td> ${resultado.estado}</td>
             

               <td>
      
               <button   class ="btn btn-sm boton" style="border: none"  value="${id}"> 
             <i class="fas fa-plus" data-bs-toggle="tooltip" title="Tomar pedido"></i>
               </button>
       
                 </td>
     
        </tr>
       
        
        `;

    });


     $("#listaPedido").html(template);
}


function  templatePersonal (resultado) {




  let template = "";


  fecha=" ";
  template += ` 
  <table class="table table-striped table-hover">
  <thead>
    <tr>
      <th width="8%">#</th>
      <th width="25%">Fecha</th>
      <th width="25%">Hora</th>
      <th width="25%">Estado</th>
     
    </tr>
  </thead>
  <tbody >

  
       
        `;


  resultado.forEach(resultado => {

   
      resultado['estado']=status(resultado['estado']);
      A= resultado.idCarrito;
      id=window.btoa(A);
      fecha=resultado.fechaCompra.split(" ");
      template += `
      <tr>
             <td>4825-2-${resultado.idCarrito}</td>
             <td>${fecha[0]}</td> 
             <td> ${fecha[1]}</td>
             <td> ${resultado.estado}</td>
           

     
             </tr>
      `;


  });
  template += `
  </tbody>
  </table>
      `;
      

   $("#listaPedido").html(template);
}




function  templatePedido (resultado) {
  let template = "";


  fecha=" ";
  template += ` 
  
  <div class="table-responsive">
<table class="table text-nowrap">
<thead class="thead-dark">
  <tr>
    <th scope="col">Codigo</th>
    <th scope="col">Fecha de compra</th>
    <th scope="col">Direccion de entrega</th>
  </tr>
</thead>
<tbody>

  
       
        `;




   
      resultado['estado']=status(resultado['estado']);
      A= resultado.idCarrito;
      id=window.btoa(A);
      fecha=resultado.fechaCompra.split(" ");




      var meta= resultado.Cliente_idCliente.split(":");

      template += `
      
      <tr>
      <td>4825-2-${resultado.idCarrito}</td>
      <td>${fecha[0]} </td> 
        <td>${meta[1]}</td>
      </tr>
      `;



  template += `
  </tbody>
  </table>
  </div>
  <button type="button" id="fin_pedido" class="btn btn-primary btn-block mb-3"  value="${id}" >Completar Entrega</button>


      `;
      

   $("#listaPedido").html(template);
}




function status(estado)
{
switch (estado) {
  case "0":
    return "Cancelado";
  case "1":
      return "Por Asignar";
  
    case "2":
      return"En trasportadora";

    case "3":
      return"Pedido entregado";

     

 
}



}


function tabla(guia) {
    switch (guia) {
      case 0:
      return 0;
     

        case 1:
        return 1;
      

          case 2:
        return 2;

        
        case 3:
          return 3;
           
    
     
    }
}
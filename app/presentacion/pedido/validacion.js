$(document).ready(function () {
    

    mostrar();

    $(document).on("click", ".validar", function (e) {
      
        e.preventDefault();
        var guia = window.atob($(this).val());
        console.log(guia);
        var meta= guia.split(":");


        var data = {
            "idPedido": meta[0],
            "idCarrito": meta[1],
        };
        var opcion="validar";

        Swal.fire({
            title: "Estas seguro?",
            text: "¿Desea validar este pedido?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Si, añadirlo!",
            cancelButtonText: "Cancelar!",
          })
          
          
          .then((result) => {
      
            
            if (result.isConfirmed) {
          
          $.ajax({
            type: "post",
            url: "app/servicios/generales/pedido/ajax_validar_pedido.php",
            data: { data,opcion },
      
            success: function (response) {
      
                console.log(response);
              

             if(response>0)
             {
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'El pedido ha sido añadido',
                showConfirmButton: false,
                timer: 1500
              })
              mostrar();
      
             }else
             {
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Complete el pedido actual para poder tomar otro',
                showConfirmButton: false,
                
              })
             }
          
      
      
            },
           
          });
      
        }

      });

   

    });


});



function mostrar() {

    $(".pag").show();
    $("#cantidad").show();
  
 
      var cantidad = document.getElementById("cantidad").value;
      var pagina = document.getElementById("pagina").value;
  
    var opcion ="leer";
    var meta= 2;
   
      $.ajax({
          type: "post",
          url: "app/servicios/generales/pedido/ajax_validar_pedido.php",
          data: {
              cantidad ,pagina,opcion,meta
          },
         
          success: function (response) {
        //    template(response.datos);
             var data =response.datos

              template ="";
                  data.forEach(item => {
   
              
              
                    var meta=item.Domiciliario_idDomiciliario.split(":");
              
                    var value = window.btoa(item.idPedido+":"+item.carrito);
                   
                      template += `
                          <tr>
                              <td>1013-1-${item.idPedido} </td>
                              <td>4825-2-${item.carrito} </td>
                              <td> ${meta[1]} </td>
                           
                              <td><button type="button" class="btn btn-danger validar" value="${value}">Validar pedido</button> </td>
                          </tr>
                      `;
                  });
                 
              
                  $("#listaPedido").html(template);


          }
      });
      
  }

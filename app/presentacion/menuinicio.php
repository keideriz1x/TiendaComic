<?php
// require_once 'persistencia/modeloDAO/Categoria.php';

?>

<nav class="menu navbar navbar-expand-lg navbar-dark bg-danger">
  <a class="navbar-brand" href="index.php"><img src="app/img/jd.png" width="150px"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class=" pt-2 mx-auto d-block" style="width: 100%">
    <div class="input-group">
      <div class="input-group-prepend">
        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span style="font-size: calc(0.9% + 0.9em)">Categoria</span>
        </button>

        <ul class="dropdown-menu" role="menu">
          <?php
          $categoria = (new Categoria())->get("nombre");
          foreach ($categoria as $clave => $valor) {
          ?> <a class="dropdown-item" href="#"> <?php echo "$valor->nombre "; ?></a> <?php
                                                                                    }
                                                                                      ?>
        </ul>
      </div>
      <input type="text" class="form-control" aria-label="Text input with dropdown button">
      <span class="input-group-prepend">
        <button class="btn btn-primary" type="button"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-search" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z" />
            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z" />
          </svg></button>
      </span>
      <div class="input-group-btn">
        <ul class="navbar-nav pt-1 pl-3 pr-1">
          <li>
            <a href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoCliente.php") ?>&priv=<?php echo base64_encode(base64_encode("carrito")) ?>" style='text-decoration:none'>
              <i style='color: white;' class='fas fa-cart-arrow-down fa-2x'></i>
              <span class='car px-1 float-right'><b>
                  <?php
                  echo (empty($_SESSION["carrito"])) ? 0 : count($_SESSION["carrito"]);
                  ?>
                  <!-- <span id="cantidadProducto"> </span> -->
                </b></span></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
    <ul class="navbar-nav ">
      <li class="nav-item active my-auto pl-2 pr-2">
        <a class='nav-link' href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoCliente.php") ?>&priv=<?php echo base64_encode(base64_encode("pedido")) ?>">
          <div class='nav-line-1-container'><span class='nav-line-1'>Pedidos</span></div>
        </a>
      </li>
      <?php if (isset($_SESSION["id"]) && $_SESSION["rol"] == "cliente") {
        $nombre = $_SESSION["datos"]->nombre;
        $apellido = $_SESSION["datos"]->apellido; ?>
        <li class="nav-item active d-none d-md-none d-lg-block my-auto">

          <?php
          if ($_SESSION["datos"]->foto == null) {
          ?>
            <i class="fas fa-user-circle fa-2x pt-1" style="color: white;">
            <?php
          } else {
            ?>
              <img src="data:image/png;base64,<?php echo $_SESSION["datos"]->foto ?>" class="menuPerfil mx-auto d-block ">
            <?php
          }
            ?>
            </i> <!-- Foto aqui, tamaño de foto de 2em -->
        </li>
        <li class="nav-item dropdown active">
          <a class="nav-link dropdown-toggle" id="inicio" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="nav-line-1-container"><span class="nav-line-1"><?php echo " " . $nombre . " " ?></span></div>
            <span class="nav-line-2 nav-long-width"><b><?php echo " " . $apellido ?></b><span class="nav-icon nav-arrow null" style="visibility: visible;"></span>
            </span>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("app/presentacion/permisoCliente.php") ?>&priv=<?php echo base64_encode(base64_encode("perfilCliente")) ?>">Editar Perfil</a>
            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("app/presentacion/logout.php") ?>">Cerrar Sesión</a>
          </div>
        </li>
      <?php } else { ?>
        <li class="nav-item active d-none d-md-none d-lg-block my-auto pl-1">
          <i class="fas fa-user-circle fa-2x pt-1" style="color: white;"></i>
        </li>
        <li class="nav-item active my-auto pl-2">
          <a class="nav-link" id="inicio" href="#" data-toggle="modal" data-target="#exampleModal">
            <div class="nav-line-1-container text-nowrap"><span class="nav-line-1">Hola, Identifícate</span></div>
          </a>
        </li>
      <?php } ?>
    </ul>

  </div>
</nav>

<div class="modal fade pt-5" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header d-block">
          <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h5 class="modal-title text-center" id="exampleModalLabel">Iniciar Sesión</h5>
        </div>
        <div class="modal-body">
          <form action=<?php echo "index.php?pid=" . base64_encode("app/presentacion/autenticar.php") ?> method="post">
            <div class="form-group">
            <input type="email" name="correo" maxlength="30" minlength="14" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" class="form-control" placeholder="Correo" required="required">
            </div>
            <div class="form-group">
              <input type="password" name="clave" minlength="4"  maxlength="15" class="form-control" placeholder="Clave" required="required">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-outline-primary btn-block">Autenticar</button>
            </div>
            <p class="text-center">
              <a href="<?php echo "index.php?pid=" . base64_encode("app/presentacion/excepciones.php") . "&priv=" . base64_encode(base64_encode("clave")) ?>">¿Olvido su contraseña?</a>
            </p>
            <p class="text-center">
              <a>o</a>
            </p>
            <p class="text-center">
              <a href="<?php echo "index.php?pid=" . base64_encode("app/presentacion/excepciones.php") . "&priv=" . base64_encode(base64_encode("crear")) ?>">Crear una cuenta</a>
            </p>
          </form>
        </div>
      </div>
    </div>
  </div>
  
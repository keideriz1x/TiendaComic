<?php
include "app/presentacion/menuinicio.php";

?>
<div class="col-lg-3 container pt-5">
    <div class="card">
        <div class="cardCl card-header text-center text-white rounded">
            Crear una cuenta
        </div>
        <div class="card-body text-center">
        <form  id="clave" action=<?php echo "index.php?pid=" . base64_encode("app/presentacion/crearCliente.php") ?> method="post">
						<div class="form-group">
							<input type="text" name="nombre" class="form-control" placeholder="Nombre" required="required" autocomplete="off" maxlength="20" minlength="3">
                        </div>
                        <div class="form-group">
							<input type="text" name="apellido" class="form-control" placeholder="Apellido" required="required" autocomplete="off" maxlength="20" minlength="3">
                        </div>
                        <div class="form-group">
                            <input type="email" name="correo" class="form-control" placeholder="Correo electronico" maxlength="30" minlength="14" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" size="30" required autocomplete="off">
                        </div>
                        <div class="form-group">
                            <input type="email" name="cCorreo" class="form-control" placeholder="Confirmar Correo electronico" maxlength="30" minlength="14" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" size="30" required autocomplete="off">
                        </div>
                        <div class="form-group">
							<input type="password" name="clave" class="form-control" placeholder="Contraseña" required="required" autocomplete="off" minlength="4"  maxlength="15">
                        </div>
                        <div class="form-group">
							<input type="password" name="cClave" class="form-control" placeholder="Confirmar Contraseña" required="required" autocomplete="off" minlength="4"  maxlength="15">
						</div>
						<div class="form-group text-center">
					<button type="submit" id="enviar" class="btn btn-primary btn-block">Enviar</button>
				</div>
				</form>

            <?php
                if (isset($_SESSION["errorC"]) && !empty($_SESSION["errorC"])) {
            ?>
            <script>
			Swal.fire({
				icon: 'error',
				title: 'Oops...',
				text: ' <?php echo $_SESSION["errorC"] ?>'
				
			})
	            	</script>

            <?php } ?>


        </div>
    </div>
</div>
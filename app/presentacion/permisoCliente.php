<?php
if (isset($_SESSION["id"])) {
    if ($_SESSION["rol"] == "cliente") {
        include "app/presentacion/menuinicio.php";
        switch (base64_decode(base64_decode($_GET["priv"]))) {
            case "carrito":
                include "app/presentacion/carrito.php";
                break;
            case "pedido":
                include "app/presentacion/pedido.php";
                break;
            case "perfilCliente":
                include "app/presentacion/editPerfilCliente.php";
                break;
            case "compra":
                    include "app/presentacion/compra.php";
                    break;
                default:
                include "app/presentacion/noExiste.php";
                break;
        }
    ?>
    <?php
    }elseif ($_SESSION["rol"] == "administrador" || $_SESSION["rol"] == "domiciliario") {
        if ($_SESSION["rol"] == "administrador") {
            include "app/presentacion/menuAdministrador.php";
            include "app/presentacion/permisos.php";
        }
        elseif ($_SESSION["rol"] == "domiciliario") {
            include "app/presentacion/menuDomiciliario.php";
            include "app/presentacion/permisos.php";
        }
        
    }
?>
<?php
}else{
    include "app/presentacion/sinSesion.php";
}
?>

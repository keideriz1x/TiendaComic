<?php
require_once (dirname(__FILE__).'/../../../constantes/EMensaje.php');
require_once (dirname(__FILE__).'/../../../logica/Respuesta.php');
require_once (dirname(__FILE__).'/../../../conexion/Conexion.php');
require_once (dirname(__FILE__).'/../../../persistencia/Crud.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/ModeloGenerico.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Domiciliario.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorDomiciliario.php');

$busqueda= $_POST['busqueda'];

if(!empty($busqueda)){
    
    $domiciliario= new ControladorDomiciliario();
    $id= $domiciliario->buscarDomiciliario($busqueda);
    echo $id->json();

}
<?php
require_once (dirname(__FILE__).'/../../../constantes/EMensaje.php');
require_once (dirname(__FILE__).'/../../../logica/Respuesta.php');
require_once (dirname(__FILE__).'/../../../conexion/Conexion.php');
require_once (dirname(__FILE__).'/../../../persistencia/Crud.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/ModeloGenerico.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Domiciliario.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorDomiciliario.php');



if(empty($_POST["cantidad"]) && empty($_POST["pagina"]))
{
  $cantidad=5;
  $pagina=1;
}else{
  
$cantidad =  $_POST["cantidad"]  ; 
$pagina =  $_POST["pagina"]; 
}


$domiciliario = new ControladorDomiciliario();
$id=$domiciliario->listarPag($cantidad, $pagina, " ", "");

if($id->getCodigo()>0)
{
     echo $id->json();
}

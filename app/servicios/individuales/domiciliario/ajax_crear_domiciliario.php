<?php
require_once (dirname(__FILE__).'/../../../constantes/EMensaje.php');
require_once (dirname(__FILE__).'/../../../logica/Respuesta.php');
require_once (dirname(__FILE__).'/../../../conexion/Conexion.php');
require_once (dirname(__FILE__).'/../../../persistencia/Crud.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/ModeloGenerico.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Domiciliario.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorDomiciliario.php');


include_once (dirname(__FILE__).'/../../../servicios/generales/log/log.php');


if(isset($_POST["nombre"]))
{
    $domiciliario= new ControladorDomiciliario();
       
        $clave = password_hash($_POST["clave"],PASSWORD_BCRYPT );
      
        $id=$domiciliario->insertarDomiciliario(
            [
                "nombre"=>$_POST["nombre"],
                "apellido"=>$_POST["apellido"],
                "correo"=>$_POST["correo"],
                "clave"=>$clave,
                "estado"=>$_POST['estado'],
                "foto"=>$_POST["foto"]
            ] 
        );
        $info=$id->getDatos();

        $descripcion=base64_encode("Inserción: Creación de domiciliario con identificador <u>".$info."</u>" );

    if($id->getCodigo()==1)

        registro("Insertar","Domiciliario",$_SESSION["id"],$descripcion);
    else
        echo -1;
        
    



}
<?php
require_once (dirname(__FILE__).'/../../../../constantes/EMensaje.php');
require_once (dirname(__FILE__).'/../../../../logica/Respuesta.php');
require_once (dirname(__FILE__).'/../../../../conexion/Conexion.php');
require_once (dirname(__FILE__).'/../../../../persistencia/Crud.php');
require_once (dirname(__FILE__).'/../../../../persistencia/modeloDAO/ModeloGenerico.php');
require_once (dirname(__FILE__).'/../../../../persistencia/modeloDAO/Producto.php');
require_once (dirname(__FILE__).'/../../../../logica/ControladorProducto.php');

require_once (dirname(__FILE__).'/../../../../persistencia/modeloDAO/Categoria.php');
require_once (dirname(__FILE__).'/../../../../logica/ControladorCategoria.php');

require_once (dirname(__FILE__).'/../../../../persistencia/modeloDAO/Tipo.php');
require_once (dirname(__FILE__).'/../../../../logica/ControladorTipo.php');

$data= [];

//cargar datos tipo
            $type = new ControladorTipo();
					$id=$type->listarTipo();
					$tipo= $id->getDatos();


  $data["listaTipo"]=$tipo;


  //cargar datos categoria 

            $category = new ControladorCategoria();
                    $id=$category->listarCategoria();
                    $categoria= $id->getDatos();

  $data["listaCategoria"]=$categoria;

// carga de datos producto
$producto= new ControladorProducto();

$dato =$producto ->cargarInformacion($_POST["data"]);

$value= $dato->getDatos();
    
$value->foto =base64_encode($value->foto);



$data["producto"]=$value;



//carga datos extra

$nombreTipo= $type->nombreTipo($value->Tipo_idTipo);
$data["nombreTipo"]=$nombreTipo->getDatos();

$nombreCategoria= $category->nombreCategoria($value->Categoria_idCategoria);
$data["nombreCategoria"]=$nombreCategoria->getDatos();

echo json_encode($data);





//  echo $dato->json();



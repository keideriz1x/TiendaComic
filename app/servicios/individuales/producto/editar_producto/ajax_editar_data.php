<?php

require_once (dirname(__FILE__).'/../../../../constantes/EMensaje.php');
require_once (dirname(__FILE__).'/../../../../logica/Respuesta.php');
require_once (dirname(__FILE__).'/../../../../conexion/Conexion.php');
require_once (dirname(__FILE__).'/../../../../persistencia/Crud.php');
require_once (dirname(__FILE__).'/../../../../persistencia/modeloDAO/ModeloGenerico.php');
require_once (dirname(__FILE__).'/../../../../persistencia/modeloDAO/Producto.php');
require_once (dirname(__FILE__).'/../../../../logica/ControladorProducto.php');

require_once (dirname(__FILE__).'/../../../../persistencia/modeloDAO/Categoria.php');
require_once (dirname(__FILE__).'/../../../../logica/ControladorCategoria.php');

require_once (dirname(__FILE__).'/../../../../persistencia/modeloDAO/Tipo.php');
require_once (dirname(__FILE__).'/../../../../logica/ControladorTipo.php');





if(!isset($_SESSION))
session_start();


if(isset($_POST))
{
    
include_once (dirname(__FILE__).'/../../../../servicios/generales/log/log.php');
    $actualizacion= [];


    if (isset($_FILES['foto']  ) ) {
    
        $imagen= $_FILES['foto'];
        $tipo = $imagen['type'];
        $size =$imagen['size'];
        
        
        if($tipo == "image/jpeg" || $tipo == "image/png"){
        
        
            $hola= (file_get_contents($_FILES['foto']['tmp_name']));
    
           
            $actualizacion["foto"]=$hola;
    
            
          
        }
    
      
    } 

    $actualizacion["idProducto"]=$_POST["id"];
    
    if(isset($_POST["nombre"]  ) && !empty($_POST["nombre"])){
        $actualizacion["nombre"]=$_POST["nombre"];
    }
    
    if(isset($_POST["precio"]  ) && !empty($_POST["precio"])){
        $actualizacion["precio"]=$_POST["precio"];
    }

    
    if(isset($_POST["cantidad"]  ) && !empty($_POST["cantidad"])){
        $actualizacion["stock"]=$_POST["cantidad"];
    }

    if(isset($_POST["descripcion"]  ) && !empty($_POST["descripcion"])){
        $actualizacion["descripcion"]=$_POST["descripcion"];
    }


    $actualizacion["Tipo_idTipo"]=$_POST["tipo"];
    $actualizacion["Categoria_idCategoria"]=$_POST["categoria"];

    $rol= new ControladorProducto();

    $id=$rol->actualizar($actualizacion);

   

    if($id->getCodigo()==1)
    {
        
          $descripcion=base64_encode("Actualización: Edición de producto con identificador <u>".$_POST["id"]."</u>" );
        registro("Actualizar","Producto",$_SESSION["id"],$descripcion);
    }else
        echo -1;

}


else
echo -1;

//aadad





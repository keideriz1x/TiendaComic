<?php

require_once (dirname(__FILE__).'/../../../constantes/EMensaje.php');
require_once (dirname(__FILE__).'/../../../logica/Respuesta.php');
require_once (dirname(__FILE__).'/../../../conexion/Conexion.php');
require_once (dirname(__FILE__).'/../../../persistencia/Crud.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/ModeloGenerico.php');

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Tipo.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorTipo.php');



$busqueda= $_POST['busqueda'];

if(!empty($busqueda)){
    $tipo= new ControladorTipo();
    $id= $tipo->buscarTipo($busqueda);
    echo $id->json();

}
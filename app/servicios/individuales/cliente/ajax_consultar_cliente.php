<?php
require_once (dirname(__FILE__).'/../../../constantes/EMensaje.php');
require_once (dirname(__FILE__).'/../../../logica/Respuesta.php');
require_once (dirname(__FILE__).'/../../../conexion/Conexion.php');
require_once (dirname(__FILE__).'/../../../persistencia/Crud.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/ModeloGenerico.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Cliente.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorCliente.php');

$busqueda= $_POST['busqueda'];

if(!empty($busqueda)){
    
    $id=$cliente= (new ControladorCliente())->buscarCliente($busqueda);
    echo $id->json();
}
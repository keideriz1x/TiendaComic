<?php
require_once (dirname(__FILE__).'/../../../constantes/EMensaje.php');
require_once (dirname(__FILE__).'/../../../logica/Respuesta.php');
require_once (dirname(__FILE__).'/../../../conexion/Conexion.php');
require_once (dirname(__FILE__).'/../../../persistencia/Crud.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/ModeloGenerico.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Categoria.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorCategoria.php');



$busqueda= $_POST['busqueda'];

if(!empty($busqueda)){
    $categoria= new ControladorCategoria();
    $id= $categoria->buscarCategoria($busqueda);
    echo $id->json();

}


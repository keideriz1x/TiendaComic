<?php


if(!isset($_SESSION))
session_start();

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Log.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorLog.php');

function registro($accion,$actor,$autor,$descripcion){
    $registro = [];

    $registro["accion"]=$accion;
    $registro["actor"]=$actor;
    $registro["Administrador_idAdministrador"]=$autor;
    $registro["fechayhora"]= date("Y-m-d H:i:s");
    $registro["ip"]="no añadido";
    $registro["SO"]="no añadido";
    $registro["descripcion"]=$descripcion;

    $reg= new ControladorLog();
    $id=$reg->insertarRegistro($registro);
    echo $id->getMensaje();

}
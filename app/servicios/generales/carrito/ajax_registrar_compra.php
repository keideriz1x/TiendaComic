<?php

require_once (dirname(__FILE__).'/../../../constantes/EMensaje.php');
require_once (dirname(__FILE__).'/../../../logica/Respuesta.php');
require_once (dirname(__FILE__).'/../../../conexion/Conexion.php');
require_once (dirname(__FILE__).'/../../../persistencia/Crud.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/ModeloGenerico.php');


require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Carrito.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorCarrito.php');


require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Producto_has_carrito.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorProductoCarrito.php');


require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Producto.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorProducto.php');

if(!isset($_SESSION))
session_start();

// crea carrito en BD

if(!isset($_SESSION["carrito"]) || empty($_SESSION["carrito"]))
{
  echo -1;
}
else
{
$registro=array(
    "estado"=>1,
    "Cliente_idCliente"=>$_SESSION["id"],
    "fechaCompra"=>$fecha=date("Y-m-d H:i:s")

);


$carrito= new ControladorCarrito();
$info=$carrito->insertarCarrito($registro);
$id=  $info->getDatos();

//registra producto comprados enlazdos al carrito

$product = new ControladorProducto();
$has = new ControladorProductoCarrito();
foreach($_SESSION["carrito"] as $indice=> $producto){
    
    $p=$producto["idProducto"];
    $cantidad =  $producto["cantidad"];
    
  $data= [
    "Producto_idProducto"=>$p,
    "Carrito_idCarrito"=>$id,
    "cantidad"=>$cantidad
  ];

  $has->insertarHas($data);

  $x=$product->stock($p);

$value=$x->getDatos();
 $editor= intval($value->stock)-intval($cantidad);
 $product->actualizar([
    "idProducto"=>$p,
    "stock"=>$editor,
 ]);

   
};


foreach($_SESSION["carrito"] as $indice=> $producto){
          unset($_SESSION["carrito"][$indice] );
};
echo 1;
}




















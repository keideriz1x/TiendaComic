<?php

require_once (dirname(__FILE__).'/../../../constantes/EMensaje.php');
require_once (dirname(__FILE__).'/../../../logica/Respuesta.php');
require_once (dirname(__FILE__).'/../../../conexion/Conexion.php');
require_once (dirname(__FILE__).'/../../../persistencia/Crud.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/ModeloGenerico.php');



require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Administrador.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorAdministrador.php');

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Cliente.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorCliente.php');

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Domiciliario.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorDomiciliario.php');

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Tipo.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorTipo.php');


require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Categoria.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorCategoria.php');


include_once (dirname(__FILE__).'/../../../servicios/generales/log/log.php');



switch ($_POST["data"][0]) {
    case 'client':
       $rol=new ControladorCliente();
        cambiar_estado($rol,"idCliente","Cliente");

        break;
    case 'roundsman':
        $rol=new ControladorDomiciliario();
        cambiar_estado($rol,"idDomiciliario","Domiciliario");
        break;
    case 'tipo':
        $rol=new ControladorTipo();
        cambiar_estado($rol,"idTipo","Tipo");
        break; 
    case 'categoria':
        $rol=new ControladorCategoria();
        cambiar_estado($rol,"idCategoria","Categoria");
        break;   
     
}

function cambiar_estado($rol,$equiparador,$actor){
    $estado=$_POST["data"][2];
    echo $_POST["data"][2];
    $rest = substr($equiparador, 2);
    if($estado=="Activo")
    {

        $id=$rol->actualizar([
            $equiparador=>$_POST["data"][1],
            "estado"=>0
        ]);
        $descripcion=  base64_encode("<b>Cambio de estado:</b> <u>".$rest."</u> con identificador <u>".$_POST["data"][1]."</u> fue Restringido") ;
     
        registrar($id,$actor,"Deshabilitar",$descripcion);


    }else{
        $id=$rol->actualizar([
            $equiparador=>$_POST["data"][1],
            "estado"=>1
        ]);
        $descripcion=  base64_encode("<b>Cambio de estado:</b> <u>".$rest."</u>  con identificador <u>".$_POST["data"][1]."</u> fue Habilitado") ;
        registrar($id,$actor,"Habilitar",$descripcion);
    }
  
}

function registrar($verificador,$actor,$accion,$descripcion)
{
    if($verificador->getCodigo()==1){
    registro($accion,$actor,$_SESSION["id"],$descripcion);
    echo 1;}
else
    echo -1;
}
<?php
require_once (dirname(__FILE__).'/../../../constantes/EMensaje.php');
require_once (dirname(__FILE__).'/../../../logica/Respuesta.php');
require_once (dirname(__FILE__).'/../../../conexion/Conexion.php');
require_once (dirname(__FILE__).'/../../../persistencia/Crud.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/ModeloGenerico.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Administrador.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorAdministrador.php');

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Cliente.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorCliente.php');

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Domiciliario.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorDomiciliario.php');


switch ($_POST["rol"]) {
    case 'administrador':
        $rol = new ControladorAdministrador();
        carga($rol);
        break;
    case 'cliente':
        $rol = new ControladorCliente();
        carga($rol);
        break;
    case 'domiciliario':
        $rol = new ControladorDomiciliario();
        carga($rol);
        break;
    
  
}

function carga($rol)
{
    if(isset($_POST["guia"]))
    {
        
        $id=$rol->cargarInformacion($_POST["guia"]);
    
       $value= $id->getDatos();
    
       $value->foto =base64_encode($value->foto);
    
        echo $id->json();
    }

}

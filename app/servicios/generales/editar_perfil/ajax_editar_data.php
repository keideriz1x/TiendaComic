<?php
require_once (dirname(__FILE__).'/../../../constantes/EMensaje.php');
require_once (dirname(__FILE__).'/../../../logica/Respuesta.php');
require_once (dirname(__FILE__).'/../../../conexion/Conexion.php');
require_once (dirname(__FILE__).'/../../../persistencia/Crud.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/ModeloGenerico.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Administrador.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorAdministrador.php');


require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Cliente.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorCliente.php');

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Domiciliario.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorDomiciliario.php');




if(!isset($_SESSION))
session_start();

$actualizacion= [];


$rol= "";


switch ($_POST["rol"]) {
    case 'administrador':
        $actualizacion["idAdministrador"]=$_POST["id"];
        $rol= new ControladorAdministrador();
        editar($actualizacion,$rol);

        break;

        case 'cliente':
            $actualizacion["idCliente"]=$_POST["id"];
            $rol= new ControladorCliente();
            editar($actualizacion,$rol);
    
            break;
            
            
            case 'domiciliario':
                $actualizacion["idDomiciliario"]=$_POST["id"];
                $rol= new ControladorDomiciliario();
                editar($actualizacion,$rol);
        
                break;
    
    
}






function editar($actualizacion, $rol){


    if(isset($_POST["nombre"]  ) && !empty($_POST["nombre"])){
        $actualizacion["nombre"]=$_POST["nombre"];
    }
    
    if(isset($_POST["apellido"]  ) && !empty($_POST["apellido"])){
        $actualizacion["apellido"]=$_POST["apellido"];
    }

    
    if(isset($_POST["direccion"]  ) && !empty($_POST["direccion"])){
        $actualizacion["direccion"]=$_POST["direccion"];
    }

    if(isset($_POST["telefono"]  ) && !empty($_POST["telefono"])){
        $actualizacion["telefono"]=$_POST["telefono"];
    }

    
    
    if (isset($_FILES['foto']  ) ) {
    
        $imagen= $_FILES['foto'];
        $tipo = $imagen['type'];
        $size =$imagen['size'];
        
        
        if($tipo == "image/jpeg" || $tipo == "image/png"){
        
        
            $hola= (file_get_contents($_FILES['foto']['tmp_name']));
    
           
            $actualizacion["foto"]=$hola;
    
            
          
        }
    
      
    } 
    
    
    
      
        $id=$rol->actualizar($actualizacion);

        if($id->getCodigo()==1)
        {
            $_SESSION["datos"]->nombre=   $actualizacion["nombre"];
            $_SESSION["datos"]->apellido=  $actualizacion["apellido"];

            if (isset($actualizacion["telefono"]) && !empty($actualizacion["telefono"]) )
            {
                $_SESSION["datos"]->telefono=  $actualizacion["telefono"];
            }

            if (isset($actualizacion["direccion"]) && !empty($actualizacion["direccion"]) )
            {
                $_SESSION["datos"]->direccion=  $actualizacion["direccion"];
            }


             if (!empty($actualizacion["foto"]) )
            {
                $_SESSION["datos"]->foto = base64_encode($actualizacion["foto"]);
             }else
             {
                 $foto=$rol->foto($_POST["id"]);
                $value=$foto->getDatos();
                $_SESSION["datos"]->foto = base64_encode($value->foto);
             }


             echo  $id->getCodigo();

        }else echo (-1);
    

}    





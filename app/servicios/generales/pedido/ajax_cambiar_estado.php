
<?php

require_once (dirname(__FILE__).'/../../../constantes/EMensaje.php');
require_once (dirname(__FILE__).'/../../../logica/Respuesta.php');
require_once (dirname(__FILE__).'/../../../conexion/Conexion.php');
require_once (dirname(__FILE__).'/../../../persistencia/Crud.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/ModeloGenerico.php');

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Carrito.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorCarrito.php');

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Cliente.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorCliente.php');


require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Pedido.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorPedido.php');


if(!isset($_SESSION))
session_start();


$pedido= new ControladorPedido();



$id=$pedido->actualizarPerCarrito([
    
    "idCarrito"=>$_POST["data"],
    "estado"=> $_POST["estado"]

]);

echo $id->getCodigo();
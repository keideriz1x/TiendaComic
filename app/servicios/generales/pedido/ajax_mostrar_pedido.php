
<?php

require_once (dirname(__FILE__).'/../../../constantes/EMensaje.php');
require_once (dirname(__FILE__).'/../../../logica/Respuesta.php');
require_once (dirname(__FILE__).'/../../../conexion/Conexion.php');
require_once (dirname(__FILE__).'/../../../persistencia/Crud.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/ModeloGenerico.php');

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Carrito.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorCarrito.php');

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Cliente.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorCliente.php');


require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Pedido.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorPedido.php');


if(!isset($_SESSION))
session_start();

$carrito= new ControladorCarrito();

    if($_POST["rol"]>0){
        $cliente= new ControladorCliente();
        //generador de data carrito general
        if($_POST["rol"]!=2)
        {
            $id= $carrito->listarPag($_POST["rol"],$_POST["cantidad"],$_POST["pagina"],"","");
            $datos= $id->getDatos();
                    //añadir direccion de envio
                    foreach($datos as $clave=>$value)
                    {
                        $index=( $value->Cliente_idCliente);
                        $data=$cliente->cargarInformacion($index);
                        $value->Cliente_idCliente =  $value->Cliente_idCliente.":".($data->getDatos())->direccion;
                    
                    }
        }
        
        //condicion para carrito individual mediante pedido
        else
        {
            $pedido= new ControladorPedido();
                $otorgado =$pedido->pedidoOtorgado($_SESSION["id"]);
                $datos= $otorgado->getDatos();
    
            $id=$carrito->carritoOtorgado($datos->carrito);
                $data= $id->getDatos();

                    $index=( $data->Cliente_idCliente);
                    $data1=$cliente->cargarInformacion($index);
                    $data->Cliente_idCliente =  $data->Cliente_idCliente.":".($data1->getDatos())->direccion;

        }
        echo  json_encode($id->getDatos());
    }
    else
    {
        $id= $carrito->carritoPersonal($_SESSION["id"],$_POST["cantidad"],$_POST["pagina"],"","");
        echo  json_encode($id->getDatos());
    }    









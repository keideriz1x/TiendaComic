<?php
require_once (dirname(__FILE__).'/../../../constantes/EMensaje.php');
require_once (dirname(__FILE__).'/../../../logica/Respuesta.php');
require_once (dirname(__FILE__).'/../../../conexion/Conexion.php');
require_once (dirname(__FILE__).'/../../../persistencia/Crud.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/ModeloGenerico.php');

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Pedido.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorPedido.php');

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Carrito.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorCarrito.php');

if(!isset($_SESSION))
session_start();
$identificador;
$identificador=$_SESSION["id"];
$pedido= new ControladorPedido();
$entrega = $pedido->pedidoPersonal($identificador,$_POST["cantidad"],$_POST["pagina"],"","");

echo $entrega->json();
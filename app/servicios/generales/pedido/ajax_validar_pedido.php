
<?php

require_once (dirname(__FILE__).'/../../../constantes/EMensaje.php');
require_once (dirname(__FILE__).'/../../../logica/Respuesta.php');
require_once (dirname(__FILE__).'/../../../conexion/Conexion.php');
require_once (dirname(__FILE__).'/../../../persistencia/Crud.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/ModeloGenerico.php');

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Carrito.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorCarrito.php');

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Cliente.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorCliente.php');


require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Pedido.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorPedido.php');

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Domiciliario.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorDomiciliario.php');



switch (   $_POST["opcion"]) {
    case 'leer':
        leer();
        break;

    case 'validar':
        validar();
    break;
    
}



function leer(){
    $pedido=new ControladorPedido();
    $domiciliario= new ControladorDomiciliario();
    $validar= $pedido->listarPag($_POST["meta"],$_POST["cantidad"],$_POST["pagina"],"","");

            $datos= $validar->getDatos();
            foreach($datos as $clave=>$value)
            {
                $index=( $value->Domiciliario_idDomiciliario);
                $data=$domiciliario->cargarInformacion($index);
                $value->Domiciliario_idDomiciliario =  $value->Domiciliario_idDomiciliario.":".($data->getDatos())->nombre."".($data->getDatos())->apellido;
            
            }

    echo  $validar->json();
}

function validar(){
        $pedido=new ControladorPedido();
        $carrito= new ControladorCarrito();


        $estado1= $carrito->actualizar([
            "idCarrito"=> $_POST["data"]["idCarrito"],
            "estado"=>3
        ]);

        $estado2= $pedido->actualizar([
            "idPedido"=>   $_POST["data"]["idPedido"],
            "estado"=>3
        ]);


    if($estado2->getCodigo()==1)
    {
        echo 1;
    }
    else{
        echo -1; 
    }  
}


<?php

require_once (dirname(__FILE__).'/../../../constantes/EMensaje.php');
require_once (dirname(__FILE__).'/../../../logica/Respuesta.php');
require_once (dirname(__FILE__).'/../../../conexion/Conexion.php');
require_once (dirname(__FILE__).'/../../../persistencia/Crud.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/ModeloGenerico.php');
require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Administrador.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorAdministrador.php');

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Cliente.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorCliente.php');

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Domiciliario.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorDomiciliario.php');

require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Producto.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorProducto.php');


require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Categoria.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorCategoria.php');



require_once (dirname(__FILE__).'/../../../persistencia/modeloDAO/Tipo.php');
require_once (dirname(__FILE__).'/../../../logica/ControladorTipo.php');



switch ($_POST["data"][0]) {
    case 'roundsman':
        $rol= new ControladorDomiciliario();
        cargar($rol);
        break;
    
    case 'product':
        $rol= new ControladorProducto();
        cargar($rol,"producto");

        break;
}

function cargar($rol,$extra=" "){
   

    if(!empty($extra)){
            switch ($extra) {
                case 'producto':
                    $id=$rol->cargarInformacion($_POST["data"][1]);
                    $categoria = new ControladorCategoria();
                    $tipo = new ControladorTipo();


                    $value= $id->getDatos();
                    $index=intval( $value->Categoria_idCategoria);
                    $data=$categoria->nombreCategoria($index);
                    $value->Categoria_idCategoria =($data->getDatos())->nombre;
                    // obtiene a partir del id el nombre del tipo
                    $index2=intval( $value->Tipo_idTipo);
                    $data2=$tipo->nombreTipo($index2);
                    $value->Tipo_idTipo=($data2->getDatos())->nombre;
                    $value->foto =base64_encode($value->foto);
                    
                    break;
                
                default:
                $id=$rol->cargarInformacion($_POST["data"][1]);
                $value= $id->getDatos();
                $value->foto =base64_encode($value->foto);
              
                    break;
            }
            echo $id->json();
    }

   
}





